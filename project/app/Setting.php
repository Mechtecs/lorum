<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
  protected $table = "settings";

  protected $fillable = ["key", "value"];

  /**
   * @param $key string
   */
  public static function getSetting($key) {
    $setting = Setting::where("key", $key)->first();
    if (!$setting) {
      $setting = new Setting();
      $setting->key = $key;
    }
    return $setting;
  }

  /**
   * @param $key string
   * @param $value mixed | string
   * @return Setting
   */
  public static function setSetting($key, $value) {
    $setting = self::getSetting($key);
    $setting->value = $value;
    $setting->saveOrFail();
    return $setting;
  }
}
