<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Thread extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = "threads";

    protected $fillable = [
        'title'
    ];

    public function posts() {
        return $this->hasMany('App\Post', 'thread_id', 'id');
    }

    public function author() {
        if(strlen($this->user_id) < 17)
            return $this->hasOne('App\OldUser', 'id', 'user_id');
        else
            return $this->hasOne('App\User', 'steamid64', 'user_id');
    }

    public function sub_category() {
        return $this->hasOne('App\SubCategory', 'id', 'subcategory_id');
    }

    public function latestPost() {
        if(is_null($this->latest_post)) {
            $this->latest_post = $this->posts()->orderBy('created_at', 'desc')->first();
        }

        return $this->latest_post;
    }

    public function shortTitle() {
        $title = $this->title;
        if(strlen($title) > 19)
            return substr($title, 0, 19) . "...";
        else
            return $title;
    }

    public function hasAccess() {
        if($this->sub_category()->first() && $this->sub_category()->first()->hasAccess()) {
            if(Auth::guest())
                $role = Role::where('name', '=', 'user')->first();
            else
                $role = Auth::user()->roles()->first();

            $specialPerm = $this->sub_category()->first()->specialPermissionByRole($role->id)->first();

            if(!is_null($specialPerm) && $specialPerm->read_only_own) {
                if(!Auth::guest() && $this->user_id === Auth::user()->steamid64) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
