<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocCity extends Model
{
    protected $fillable = [
        'name', 'coordinates', 'accuracy_level', 'city_id'
    ];

    public function state() {
        return $this->hasOne('App\LocState', 'id', 'state_id');
    }
}
