<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketField extends Model
{
    protected $fillable = [
        'author_id',
        'ticket_id',
        'ticket_type_field_id',
        'value'
    ];

    public function author() {
        return $this->hasOne('App\User', 'steamid64', 'author_id');
    }

    public function ticket() {
        return $this->hasOne('App\Ticket', 'id', 'ticket_id');
    }

    public function type() {
        return $this->hasOne('App\TicketTypeField', 'id', 'ticket_type_field_id');
    }

    public function render() {
        return $this->type->render(true, $this->value, $this->ticket);
    }
}
