<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocState extends Model
{
    protected $fillable = [
        'name', 'coordinates', 'accuracy_level', 'state_id'
    ];

    public function country() {
        return $this->hasOne('App\LocCountry', 'id', 'country_id');
    }

    public function cities() {
        return $this->hasMany('App\LocCity', 'city_id', 'id');
    }
}
