<?php namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole {

    protected $fillable = [
        'display_name', 'username_style', 'description', 'name', 'image', 'order'
    ];

    public function allPerms() {
        $allperms = Permission::all();
        $myperms = $this->perms;

        $allpermids = array();

        foreach($allperms as $perm) {
            $perm->available = false;
            $allpermids[$perm->id] = $perm;
        }

        foreach($myperms as $perm) {
            $allpermids[$perm->id]->available = true;
        }

        return $allpermids;
    }

}
