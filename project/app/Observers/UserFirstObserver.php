<?php

namespace App\Observers;

use App\User;
use App\Role;

class UserFirstObserver {

    public function created (User $user) {
        if (User::count() === 1) {
            $adminRole = Role::where('name', '=', 'administrator')->first();
            if ($adminRole) {
                $user->attachRole($adminRole);
                $user->save();
            }
        }
    }

}