<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class TicketType extends Model
{
    protected $fillable = [
        "name",
        "description",
        "order",
        "color_open",
        "color_closed"
    ];

    protected $visible = [
        "id",
        "name",
        "description",
        "color_open",
        "color_closed",
        "order",
    ];

    public function tickets() {
        return $this->hasMany('App\Ticket', 'ticket_type_id', 'id');
    }

    public function roles() {
        return $this->hasMany('App\TicketTypeRole', 'ticket_type_id', 'id');
    }

    public function fields() {
        return $this->hasMany('App\TicketTypeField', 'ticket_type_id', 'id')->orderBy('ticket_type_fields.order', 'asc');
    }

    public function hasRole($role_id) {
        return TicketTypeRole::where('ticket_type_id', '=', $this->id)->where('role_id', '=', $role_id)->count() > 0;
    }

    public function render($ticketTypeID = null, $view = false) {
        $output = "\n\r";

        $fields = $this->fields()->get();
        foreach($fields as $field) {
            $output .= "<p class='field-parent-{$field->type} field-{$field->type}' id='field-parent-{$field->id}'>" . $field->render() . "</p>\n\r";
        }

        $output = "<form method='post' action='" . URL::to('/support/ticket/new') . "'>\n\r" . $output . "\n\r<input type='submit' class='btn btn-success'>\n\r";
        if($ticketTypeID)
            $output .= '<input type="hidden" name="ticketType" id="ticketType" value="' . $ticketTypeID . '" />';
        $output .= "</form>";

        return $output;
    }
}
