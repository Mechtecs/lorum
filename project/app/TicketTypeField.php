<?php

namespace App;

use App\Models\Server;
use Illuminate\Database\Eloquent\Model;

class TicketTypeField extends Model
{
    protected $fillable = [
        'name',
        'description',
        'type',
        'order'
    ];

    public function ticketType() {
        return $this->hasOne('App\TicketType', 'id', 'ticket_type_id');
    }

    public function formatView($view = false, $data, $ticket) {
        /*
         * USER WHO CREATED THE TICKET
         * {USER_STEAM2}
         * {USER_STEAM3}
         * {USER_STEAM64}
         * {USER_DISCORD_TAG}
         * {USER_KRAKEN_UID}
         * {USER_NAME}
         *
         * IF FIELD IS STEAMID
         * {FIELDID_STEAM2}
         * {FIELDID_STEAM3}
         * {FIELDID_STEAM64}
         * {FIELDID_DISCORD_TAG}
         * {FIELDID_KRAKEN_UID}
         * {FIELDID_PERSONANAME}
         *
         * IF FIELD IS SERVERID
         * {FIELDID_IP}
         * {FIELDID_PORT}
         * {FIELDID_NAME}
         * {FIELDID_KRAKEN_SID}
         * {FIELDID_KRAKEN_NAME}
         *
         * OTHERWISE {FIELDID} WILL RETURN THE VALUE OF THE FIELD
         */

        if(!$view)
            return "";

        // USER BLOCK
        if($tmp = preg_replace('/{USER_STEAM2}/', $ticket->author->steamid2(), $data))
            $data = $tmp;
        if($tmp = preg_replace('/{USER_STEAM2_SHORT}/', $ticket->author->steamid2(true), $data))
            $data = $tmp;
        if($tmp = preg_replace('/{USER_STEAM3}/', $ticket->author->steamid3(), $data))
            $data = $tmp;
        if($tmp = preg_replace('/{USER_STEAM64}/', $ticket->author->steamid64(), $data))
            $data = $tmp;
        if($tmp = preg_replace('/{USER_DISCORD_TAG}/', $ticket->author->discord_username . '#' . $ticket->author->discord_discriminator, $data))
            $data = $tmp;
        if($tmp = preg_replace('/{USER_KRAKEN_UID}/', $ticket->author->getKrakenUID(), $data))
            $data = $tmp;
        if($tmp = preg_replace('/{USER_PERSONANAME}/', htmlspecialchars($ticket->author->personaname), $data))
            $data = $tmp;

        if($tmp = preg_replace_callback('/{([0-9]+)_STEAM2}/', function($matches) use ($ticket) {
            $steamid64 = $ticket->fields()->where('ticket_fields.ticket_type_field_id', '=', $matches[1])->first()->value;
            return User::findAlt($steamid64)->steamid2();
        }, $data))
            $data = $tmp;
        if($tmp = preg_replace_callback('/{([0-9]+)_STEAM2_SHORT}/', function($matches) use ($ticket) {
            $steamid64 = $ticket->fields()->where('ticket_fields.ticket_type_field_id', '=', $matches[1])->first()->value;
            return User::findAlt($steamid64)->steamid2(true);
        }, $data))
            $data = $tmp;
        if($tmp = preg_replace_callback('/{([0-9]+)_STEAM3}/', function($matches) use ($ticket) {
            $steamid64 = $ticket->fields()->where('ticket_fields.ticket_type_field_id', '=', $matches[1])->first()->value;
            return User::findAlt($steamid64)->steamid3();
        }, $data))
            $data = $tmp;
        if($tmp = preg_replace_callback('/{([0-9]+)_STEAM64}/', function($matches) use ($ticket) {
            return $ticket->fields()->where('ticket_fields.ticket_type_field_id', '=', $matches[1])->first()->value;
        }, $data))
            $data = $tmp;
        if($tmp = preg_replace_callback('/{([0-9]+)_DISCORD_TAG}/', function($matches) use ($ticket) {
            $steamid64 = $ticket->fields()->where('ticket_fields.ticket_type_field_id', '=', $matches[1])->first()->value;
            $user = User::findAlt($steamid64);
            return $user->discord_username . "#" . $user->discord_discriminator;
        }, $data))
            $data = $tmp;
        if($tmp = preg_replace_callback('/{([0-9]+)_KRAKEN_UID}/', function($matches) use ($ticket) {
            $steamid64 = $ticket->fields()->where('ticket_fields.ticket_type_field_id', '=', $matches[1])->first()->value;
            return User::findAlt($steamid64)->getKrakenUID();
        }, $data))
            $data = $tmp;
        if($tmp = preg_replace_callback('/{([0-9]+)_PERSONANAME}/', function($matches) use ($ticket) {
            $steamid64 = $ticket->fields()->where('ticket_fields.ticket_type_field_id', '=', $matches[1])->first()->value;
            return htmlspecialchars(User::findAlt($steamid64)->personaname);
        }, $data))
            $data = $tmp;

        if($tmp = preg_replace_callback('/{([0-9]+)_IP}/', function($matches) use ($ticket) {
            $sid = $ticket->fields()->where('ticket_fields.ticket_type_field_id', '=', $matches[1])->first()->value;
            return Server::find($sid)->ip;
        }, $data))
            $data = $tmp;
        if($tmp = preg_replace_callback('/{([0-9]+)_PORT}/', function($matches) use ($ticket) {
            $sid = $ticket->fields()->where('ticket_fields.ticket_type_field_id', '=', $matches[1])->first()->value;
            return Server::find($sid)->port;
        }, $data))
            $data = $tmp;
        if($tmp = preg_replace_callback('/{([0-9]+)_NAME}/', function($matches) use ($ticket) {
            $sid = $ticket->fields()->where('ticket_fields.ticket_type_field_id', '=', $matches[1])->first()->value;
            return Server::find($sid)->name;
        }, $data))
            $data = $tmp;

        if($tmp = preg_replace_callback('/{([0-9]+)}/', function($matches) use ($ticket) {
            return $ticket->fields()->where('ticket_fields.ticket_type_field_id', '=', $matches[1])->first()->value;
        }, $data))
            $data = $tmp;

        global $parser;
        return $parser->except('youtube')->parse($data);
    }

    public function render($view = false, $data = null, Ticket $ticket = null) {
        $field = $this;
        switch($this->type) {
            case "string":
            case "textarea":
            case "integer":
            case "date":
            case "password":
                return view('support.fields.' . $this->type, compact('field', 'view', 'data'));
            case "steamid":
            case "serverid":
                return view('support.fields.' . $this->type, compact('field', 'view', 'data'));
            case "format_view":
                return $this->formatView($view, $data, $ticket);
            default:
                return "<p>Field type: " . $this->type . "</p>";
        }
    }
}
