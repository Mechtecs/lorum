<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BanHistory extends Model
{
    protected $table = "ban_history";

    protected $fillable = [
        'victim_id', 'judge_id',
        'from', 'to'
    ];

    public function victim() {
        return $this->hasOne('App\User', 'steamid64', 'victim_id');
    }

    public function judge() {
        return $this->hasOne('App\User', 'steamid64', 'judge_id');
    }

    public function active() {
        return !Carbon::createFromFormat('Y-m-d H:i:s', $this->to)->isPast();
    }
}
