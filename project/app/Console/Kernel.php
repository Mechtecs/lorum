<?php

namespace App\Console;

use App\Console\Commands\MyBB_Migration;
use App\Models\Server;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use xPaw\SourceQuery\SourceQuery;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        MyBB_Migration::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function() {
            $servers = Server::where("active", 1)->get();

            foreach($servers as $server) {
                $ip = $server->ip;
                $port = $server->port;
                $server_data = array();
                $query = new SourceQuery();
                try {
                    $query->Connect($ip, $port, 1, SourceQuery::SOURCE);
                    $server_data = array("info" => $query->GetInfo(), "players" => $query->GetPlayers(), "rules" => $query->GetRules(), "online" => true);
                } catch (Exception $e) {
                    return array("online" => false);
                } finally {
                    $query->Disconnect();
                }

                $map = $server_data["info"]["Map"];
                if(($cnt = count($map_exp = explode("/", $map))) > 1) {
                    $map = $map_exp[$cnt - 1];
                    $server_data["info"]["Map"] = $map;
                }

                Cache::put('server_status_' . $ip . '_' . $port, $server_data, 5);
            }
        })->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
