<?php

namespace App\Console\Commands;

use App\Category;
use App\OldUser;
use App\Post;
use App\Role;
use App\SubCategory;
use App\Thread;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MyBB_Migration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mybb:migrate {database} {prefix} {--flush}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'MyBB to Lorum migration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $db = $this->argument('database');
        $prefix = $this->argument('prefix');
        $pre = $db . "." . $prefix;

        $db = env('DB_DATABASE');

        if($this->option('flush')) {
            foreach(explode(";", "SET SESSION FOREIGN_KEY_CHECKS = 0;TRUNCATE $db.categories;TRUNCATE $db.category_role;TRUNCATE $db.old_users;
            TRUNCATE $db.posts;TRUNCATE $db.subcategories;TRUNCATE $db.subcategory_role_permissions;TRUNCATE $db.threads") as $item) {
                DB::statement($item.';');
            }
        }

        $fakeuser = User::updateOrCreate([
            'steamid64' => '76413371337133769'
        ], [
            'personaname' => 'Fake :('
        ]);

        $categories_raw = DB::select(DB::raw("SELECT * FROM " . $pre . "forums WHERE type = 'c'"));
        $subcategories_raw = DB::select(DB::raw("SELECT * FROM " . $pre . "forums WHERE type = 'f'"));

        $categories = array();
        $subcategories = array();

        foreach($categories_raw as $category_raw) {
            if(strlen($category_raw->linkto) > 0)
                continue;
            $cat = new Category();
            $cat->fill(array(
                "title" => $category_raw->name,
                "description" => $category_raw->description,
                "order" => $category_raw->disporder,
            ));
            $cat->save();
            $categories[$category_raw->fid] = $cat;
        }

        foreach($subcategories_raw as $subcategory_raw) {
            if(strlen($category_raw->linkto) > 0)
                continue;
            $subcat = new SubCategory();
            $subcat->fill(array(
                "title" => $subcategory_raw->name,
                "description" => $subcategory_raw->description,
                "order" => $subcategory_raw->disporder,
            ));
            $subcat->category_id = $categories[intval(explode(',', $subcategory_raw->parentlist)[0])]->id;
            $subcat->save();
            $subcategories[$subcategory_raw->fid] = $subcat;
        }

        $users_raw = DB::select(DB::raw("SELECT * FROM " . $pre . "users"));

        $users = array();

        foreach($users_raw as $user_raw) {
            $user_raw = (array) $user_raw;
            $olduser = new OldUser();
            $olduser->fill($user_raw);
            $olduser->saveOrFail();
            if(strlen($user_raw["steamID64"]) === 17) {
                $newuser = User::updateOrCreate(array(
                    'steamid64' => $user_raw["steamID64"]
                ), array(
                ));

                $olduser->linked = true;
                $olduser->steamid64 = $newuser->steamid64;
                $olduser->id = $newuser->steamid64;

                if($newuser->wasRecentlyCreated) {
                    $userrole = Role::where('name', '=', 'user')->first(); //TODO: fix hardcoded normal user role
                    $newuser->attachRole($userrole);
                    $newuser->personaname = $user_raw["username"];
                    $newuser->saveOrFail();
                }
            }
            $users[$user_raw["uid"]] = $olduser;
        }

        $threads_raw = DB::select(DB::raw("SELECT tid, fid, subject, uid, dateline, lastpost, visible FROM " . $pre . "threads"));

        $threads = array();

        foreach($threads_raw as $thread_raw) {
            $thread = new Thread();
            $thread->fill(array(
                "title" => $thread_raw->subject
            ));
            if(!array_key_exists($thread_raw->uid, $users))
                continue;
            $thread->user_id = $users[$thread_raw->uid]->id;
            $thread->subcategory_id = $subcategories[$thread_raw->fid]->id;
            $thread->created_at = Carbon::createFromTimestampUTC($thread_raw->dateline);
            $thread->updated_at = Carbon::createFromTimestampUTC($thread_raw->lastpost);
            $thread->saveOrFail();
            $threads[$thread_raw->tid] = $thread;
            if($thread_raw->visible < 0)
                $thread->delete();
        }

        $posts_raw = DB::select(DB::raw("SELECT pid, tid, uid, message, dateline FROM " . $pre . "posts"));

        foreach($posts_raw as $post_raw) {
            $post = new Post();
            $post->content = $post_raw->message;
            if(array_key_exists($post_raw->tid, $threads))
                $post->thread_id = $threads[$post_raw->tid]->id;
            else
                continue;
            $post->user_id = $users[$post_raw->uid]->id;
            $post->created_at = Carbon::createFromTimestampUTC($post_raw->dateline);
            $post->updated_at = $post->created_at;
            $post->saveOrFail();
        }
    }
}
