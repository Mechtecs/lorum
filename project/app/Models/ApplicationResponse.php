<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ApplicationResponse extends Model {

    protected $table = "application_messages";

    protected $fillable = [
        'message'
    ];

    public function applications() {
        return $this->hasOne('App\Models\Application');
    }

    public function user() {
        return $this->hasOne('App\User', 'steamid64', 'author_id');
    }

    public function readByUser(User $user) {
        return DB::select(DB::raw('SELECT count(*) AS aggregate FROM application_message_read WHERE user_id = "' . $user->steamid64 . '" AND application_message_id = ' . $this->id))[0]->aggregate > 0;
    }

    public function read(User $user) {
        if(!$this->readByUser($user)) {
            DB::insert(DB::raw('INSERT INTO application_message_read (user_id, application_message_id) VALUES ("' . $user->steamid64 . '", ' . $this->id . ')'));
        }
    }

}
