<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServerGroup extends Model {

    protected $table = "server_groups";

    protected $fillable = ['order', 'name', 'active'];

    public function servers() {
        return $this->hasMany('App\Models\Server');
    }

}
