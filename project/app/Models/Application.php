<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Application extends Model {

    protected $table = "applications";

    protected $fillable = [
        'languages', 'location', 'birthday', 'text1', 'text2', 'text3', 'text4', 'text5', 'text6', 'text7'
    ];

    public function responses() {
        return $this->hasMany('App\Models\ApplicationResponse', 'application_id', 'id');
    }

    public function author() {
        return $this->hasOne('App\User', 'steamid64', 'author_id');
    }

    public function allMessagesRead($user = null) {
        if(is_null($user))
            if(Auth::guest()) {
                return true;
            } else {
                $user = Auth::user();
            }

            $allmessages = $this->responses;
            foreach($allmessages as $message) {
                if(!$message->readByUser($user))
                    return false;
            }
            return true;
    }

    public function votesFor($what) {
        return Vote::where('application_id', '=', $this->id)->where('for', '=', $what)->count();
    }
}
