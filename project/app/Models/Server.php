<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Server extends Model {

    protected $table = "servers";

    protected $fillable = ['order', 'name', 'server_group_id', 'ip', 'port', 'active'];

    public function group() {
        return $this->hasOne('App\Models\ServerGroup', 'id', 'server_group_id');
    }

}
