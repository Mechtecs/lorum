<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid', 'username', 'password', 'salt', 'loginkey', 'email'
    ];

    protected $table = "old_users";

    public function verifyPassword($password) {
        return $this->password === md5(md5($this->salt) . md5($password));
    }

    public function personaname() {
        return $this->username;
    }

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        $fakeuser = User::findOrFail('76413371337133769');
        return $fakeuser->roles();
    }

    /**
     * Check if user has a permission by its name.
     *
     * @param string|array $permission Permission string or array of permissions.
     * @param bool         $requireAll All permissions in the array are required.
     *
     * @return bool
     */
    public function can($permission, $requireAll = false)
    {
        $fakeuser = User::findOrFail('76413371337133769');
        return $fakeuser->can($permission, $requireAll);
    }

    public function isFake() {
        return true;
    }

    public function sendDiscordMessage($message) {
        return true;
    }

}
