<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketTypeRole extends Model
{
    public function ticketType() {
        return $this->hasOne('App\TicketType', 'id', 'ticket_type_id');
    }

    public function role() {
        return $this->hasMany('App\Role', 'id', 'role_id');
    }
}
