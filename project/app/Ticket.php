<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    // Relationships

    public function author() {
        return $this->hasOne('App\User', 'steamid64', 'author_id');
    }

    public function type() {
        return $this->hasOne('App\TicketType', 'id', 'ticket_type_id');
    }

    public function messages() {
        return $this->hasMany('App\TicketMessage', 'ticket_id', 'id');
    }

    public function fields() {
        return $this->hasMany('App\TicketField', 'ticket_id', 'id');
    }

    public function additionalFields() {
        return $this->type->fields()->where('ticket_type_fields.type', '=', 'format_view')->get();
    }

    // Methods

    public function assigneeName() {
        return "None";
    }

    public function scopeAccessibleTickets($query) {
        if (Auth::guest()) {
            $query->where('1', '=', '0');
        } else {
            // Allow if user opened the ticket
            $query->where('author_id', '=', Auth::user()->steamid64);

            // Allow if user is in a group of the ticket type
            $userRoles = array();
            foreach (Auth::user()->roles as $role) {
                $userRoles[] = $role->id;
            }

            $query->orWhereHas('type.roles', function ($query) use ($userRoles) {
                $query->whereIn('role_id', $userRoles);
            });
        }
    }
}
