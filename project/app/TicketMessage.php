<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketMessage extends Model
{
    protected $fillable = [
        'ticket_id',
        'author_id',
        'message'
    ];

    public function author() {
        return $this->hasOne('App\User', 'steamid64', 'author_id');
    }

    public function ticket() {
        return $this->hasOne('App\Ticket', 'id', 'ticket_id');
    }
}
