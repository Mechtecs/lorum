<?php

namespace App\Http\Controllers;

use App\Category;
use App\Role;
use App\TicketType;
use DebugBar\DebugBar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class HelpController extends Controller {

    public function index() {
        // TODO: Create a dashboard page => Also change LayoutTest to use /
        return redirect('/servers');
    }

    public function serverRules() {
        return view('rules.server');
    }

    public function randomCss() {
        \Debugbar::disable();
        $files = collect(scandir(public_path('/img/backgrounds')));
        $files = $files->splice(2);
        $image_url = URL::to('/') . '/img/backgrounds/' . $files->random();
        $ticketTypes = TicketType::all();
        return response(view('css.random', compact('image_url', 'ticketTypes')))
            ->header('Content-Type', 'text/css');
    }

    public function showServerTeam() {
        $roles = Role::orderBy('order', 'asc')->get();
        $final_roles = collect();

        foreach($roles as $role) {
            /** @var $role Role */
            foreach($role->perms as $perm) {
                if($perm->exists && $perm->name === "show_on_teamlist") {
                    $final_roles->push($role);
                    break;
                }
            }
        }

        $roles = $final_roles;

        return view("team.team", compact("roles"));
    }
}
