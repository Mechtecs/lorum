<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller {

    public function __construct() {
        if(Auth::guest())
            return back();
    }

    public function showProfile(User $user) {
        return view('profile.view', compact("user"));
    }

    public function showSettings() {
        $user = Auth::user();
        return view('profile.settings', compact("user"));
    }

}
