<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\SubCategory;
use App\Thread;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class ForumController extends Controller {

    public function index() {
        $categories = Category::with('subcategories')->get();
        return view('forum.index', compact('categories'));
    }

    public function viewSubCategory(SubCategory $subcategory) {
        if(!$subcategory->hasAccess())
            return redirect('/forum')->with('status', 'You are not allowed to see this subcategory');

        if(!Auth::guest() && Auth::user()->can('general_admin')) {
            $threads = Thread::where('subcategory_id', '=', $subcategory->id)->orderBy('updated_at', 'desc')->withTrashed()->paginate(25);
        } else {
            if ($subcategory->canOnlySeeOwn()) {
                if(Auth::guest())
                    return redirect('/forum')->with('status', 'You are not allowed to see this subcategory');
                else
                    $threads = Thread::where('user_id', '=', Auth::user()->steamid64)->where('subcategory_id', '=', $subcategory->id)->orderBy('updated_at', 'desc')->paginate(25);
            } else {
                $threads = Thread::where('subcategory_id', '=', $subcategory->id)->orderBy('updated_at', 'desc')->paginate(25);
            }
        }

        $links = $threads;

        if(!Auth::guest() && (Auth::user()->can('general_moderator') || Auth::user()->can('general_admin'))) {
            $categories = Category::with('subcategories')->get();
        }

        return view('forum.subcategory', compact('subcategory', 'threads', 'links', 'categories'));
    }

    public function myProfile() {
        if(!Auth::guest())
            return Auth::user();
    }

    public function viewThread(Thread $thread) {
        if(!$thread->hasAccess()) {
            return redirect('/forum')->with('status', 'You do not have the permissions to view a thread in this sub/category');
        }

        $posts = $thread->posts()->paginate(25);
        $links = $posts;

        return view('forum.thread.view', compact("thread", "posts", "links"));
    }

    public function saveChangesToPost(Post $post, Request $request) {
        if($post->isEditable()) {
            $post->edit_reason = $request->get('reason');
            $post->editor_id = Auth::user()->steamid64;
            $post->content = $request->get('text');
            if($post->save())
                return array("success" => true);
            else
                return array("success" => false, "reason" => "An error occured saving your input");
        } else {
            return array("success" => false, "reason" => "Not enough permissions to edit this post");
        }
    }

    public function newThread(SubCategory $subcategory) {
        if(Auth::guest()) {
            return redirect('/login');
        }

        if($subcategory->canCreateThread())
            return view('forum.thread.new', compact('subcategory'));
        else
            return back()->with('status', 'You do not have the permissions to create a new thread in this subcategory.');
    }

    public function postThread(SubCategory $subcategory, Request $request) {
        $this->validate($request, [
            'title' => 'min:2|max:64',
            'text' => 'min:10|max:16384'
        ]);

        if(Auth::guest() || !$subcategory->canCreateThread())
            return back()->with('status', 'You do not have the permissions to create a new thread in this subcategory.');

        $thread = new Thread();
        $post = new Post();

        $thread->subcategory_id = $subcategory->id;
        $thread->user_id = Auth::user()->steamid64;
        $thread->title = $request->get('title');
        $thread->save();

        $post->content = $request->get('text');
        $post->thread_id = $thread->id;
        $post->user_id = Auth::user()->steamid64;
        $post->edit_reason = "";
        $post->save();

        return redirect('/forum/view/' . $thread->id);
    }

    public function postNewReply(Request $request) {
        /** @var Thread $thread */
        $thread = Thread::find($request->get('thread_id'));
        if(is_null($thread))
            return array("success" => false, "reason" => "Could not find associated thread");
        if(!$thread->sub_category->canReplyToThreads())
            return array("success" => false, "reason" => "Not enough permissions to reply to this thread");
        $text = $request->get('text');

        $post = new Post();
        $post->content = $text;
        $post->thread_id = $thread->id;
        $post->user_id = Auth::user()->steamid64;

        $thread->author->sendDiscordMessage(Auth::user()->personaname . " replied to your thread '" . $thread->title . "'\n\rView at " . URL::to('/forum/view/' . $thread->id));

        $thread->setUpdatedAt(Carbon::now());
        $thread->save();

        if($post->save())
            return array("success" => true);
        else
            return array("success" => false, "reason", "Could not save your new post");
    }

}
