<?php

namespace App\Http\Controllers;

use App\Models\Server;
use App\Models\ServerGroup;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use xPaw\SourceQuery\SourceQuery;

class ServerController extends Controller
{
    public function index() {
        $server_groups = ServerGroup::whereActive(true)->orderBy("order", "asc")->get();

        return view('servers.servers', compact('server_groups'));
    }

    public function serverQuery(Request $request) {
        $ip = $request->get("ip");
        $port = $request->get("port");

        return Cache::remember('server_status_' . $ip . '_' . $port, 1, function () use ($ip, $port) {
            if(Server::where("ip", "=", $ip)->where("port", "=", $port)->count() === 1) {
                $server_data = array();
                $query = new SourceQuery();
                try {
                    $query->Connect($ip, $port, 1, SourceQuery::SOURCE);
                    $server_data = array("info" => $query->GetInfo(), "players" => $query->GetPlayers(), "rules" => $query->GetRules(), "online" => true);
                } catch (Exception $e) {
                    return array("online" => false);
                } finally {
                    $query->Disconnect();
                }

                $map = $server_data["info"]["Map"];
                if(($cnt = count($map_exp = explode("/", $map))) > 1) {
                    $map = $map_exp[$cnt - 1];
                    $server_data["info"]["Map"] = $map;
                }

                return $server_data;
            } else {
                return array();
            }
        });
    }
}
