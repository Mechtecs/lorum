<?php

namespace App\Http\Controllers;

use App\Category;
use App\Ticket;
use App\TicketField;
use App\TicketMessage;
use App\TicketType;
use App\TicketTypeField;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class ForumAPI extends Controller
{
  function getCategories(Request $request) {
    $categories = Category::orderBy('categories.order', 'ASC')->get();
    $data = [];

    foreach ($categories as $keyC => $category) {
      if ($category->subcategories->count() === 0 || !$category->hasAccess()) {
        $categories->forget($keyC);
        continue;
      } else {
        foreach ($category->subcategories as $keyS => $subCategory) {
          if (!$subCategory->hasAccess()) {
            $category->subcategories->forget($keyS);
          }
        }
      }

      $category = json_decode(json_encode($category), true);

      $category["subcategories"] = array_values($category["subcategories"]);

      $data[] = $category;
    }

    return $data;
  }
}
