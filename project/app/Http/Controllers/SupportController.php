<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\TicketField;
use App\TicketMessage;
use App\TicketType;
use App\TicketTypeField;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class SupportController extends Controller
{
    function parser() {
        global $parser;
        if(is_null($parser)) {
            $parser = new BBCodeParser();
            $parser->setParser('size', '/\[size\=([0-9]{1,3})\](.*?)\[\/size\]/s', '<span style="font-size: $1%;">$2</span>', '$2');
            $parser->setParser('image', '/\[img\](.*?)\[\/img\]/s', '<img src="$1" class="img-responsive">', '$i');
            $parser->setParser('hr', '/\[hr\]/s', '<hr>', '$i');
            $parser->setParser('align', '/\[align\=(center|left|right)\](.*?)\[\/align\]/s', '<span style="text-align: $1;">$2</span>', '$2');
            $parser->setParser('color-alt', '/\[color\=([#]?[a-zA-Z0-9]+)\](.*?)\[\/color\]/s', '<span style="color: $1;">$2</span>', '$2');
        }
    }

    public function index() {
        $ticketTypes = TicketType::orderBy('order', 'asc')->where('disabled', '=', false)->get();

        $roleIDs = array();

        foreach(Auth::user()->roles as $role) {
            $roleIDs[] = $role->id;
        }

        $tickets = Ticket::where('author_id', '=', Auth::user()->steamid64)->orWhere(function($query) use ($roleIDs) {
            $query->whereHas('type.roles.role', function($query) use ($roleIDs) {
                $query->whereIn('role_id', $roleIDs);
            });
        })->orderBy('created_at', 'DESC')->paginate(10);

        return view('support.index', compact('ticketTypes', 'tickets'));
    }

    public function ajaxListTickets(Request $request) {
        $roleIDs = array();

        foreach(Auth::user()->roles as $role) {
            $roleIDs[] = $role->id;
        }

        $tickets = Ticket::where(function($query) use ($roleIDs) {
            $query->where('author_id', '=', Auth::user()->steamid64)->orWhere(function($query) use ($roleIDs) {
                $query->whereHas('type.roles.role', function($query) use ($roleIDs) {
                    $query->whereIn('role_id', $roleIDs);
                });
            });
        });

        if($request->get('types')) {
            $tickets->where(function($query) use ($request) {
                $query->whereHas('type', function($query) use ($request) {
                    $query->whereIn('id', $request->get('types'));
                });
            });
        }

        if($request->get('states')) {
            $tickets->where(function($query) use ($request) {
                $query->whereIn('state', $request->get('states'));
            });
        }

        return $tickets->orderBy('created_at', 'DESC')->with('author', 'type')->paginate(10);
    }

    public function ajaxTicketTypes() {
        return TicketType::orderBy('order', 'ASC')->where('disabled', '=', false)->get();
    }

    public function ajaxRenderForm(TicketType $ticketType) {
        $this->parser();
        return $ticketType->render();
    }

    public function ajaxSearchSteamName(Request $request) {
        return DB::table("sourcemod_kraken.player_names")->where('name', 'LIKE', '%' . $request->get('query') . '%')->groupBy('steamid')->orderBy('last_used', 'desc')->select('steamid as id', 'name as text')->paginate(25); //  WHERE name LIKE ? GROUP BY steamid ORDER BY last_used DESC LIMIT 10", ['%' . $request->get('query') . '%']);
    }

    public function checkTicketAccess(Ticket $ticket, $adminOnly = false) {
        $can = false;
        if($adminOnly) {
            $roleIDs = array();
            foreach($ticket->type->roles as $roleID) {
                $roleIDs[] = $roleID->role_id;
            }

            $admin_users = User::getUsersWithRoleIDs($roleIDs);

            foreach($admin_users as $admin_user) {
                if($admin_user->steamid64 === Auth::user()->steamid64)
                    return true;
            }

            return false;
        } else {
            if($ticket->author->steamid64 === Auth::user()->steamid64 || $this->checkTicketAccess($ticket, true))
                return true;
            else
                return false;
        }
    }

    public function createTicket(Request $request) {
        if(Auth::user()->can('ban_ticket_system'))
          return back('You do not have permissions to create a new ticket!');

        $ticketType = TicketType::find($request->get('ticketType'));
        if ($ticketType->disabled) {
          return abort(401);
        } else {
          return view('support.ticket.new', compact('ticketType'));
        }
    }

    public function createTicketSave(Request $request) {
        if(Auth::user()->can('ban_ticket_system'))
            return back('You do not have permissions to create a new ticket!');
        if(!$request->get('ticketType'))
            return back()->with('status', 'Unexpected Error. Please contact @Zelenka#4001 on Discord.');
        $ticketType = TicketType::findOrFail($request->get('ticketType'));

        if ($ticketType->disabled) {
          return abort(401);
        }

        $validationRules = array();
        $validationMessages = array();
        $validationAttributes = array();

        foreach($request->except('ticketType') as $field => $content) {
            $split = explode("-", $field);
            $fieldType = TicketTypeField::whereId($split[2])->first();
            switch ($split[1]) {
                case "date":
                    $validationRules[$field] = "required";
                    $validationAttributes[$field] = $fieldType->name;
                case "integer":
                    $validationRules[$field] = "required|numeric";
                    $validationAttributes[$field] = $fieldType->name;
                case "password":
                    $validationRules[$field] = "required";
                    $validationAttributes[$field] = $fieldType->name;
                case "steamid":
                    $validationRules[$field] = "required|numeric|size:17";
                    $validationAttributes[$field] = $fieldType->name;
                case "string":
                    $validationRules[$field] = "required|max:8192";
                    $validationAttributes[$field] = $fieldType->name;
                case "textarea":
                    $validationRules[$field] = "required|max:8192";
                    $validationAttributes[$field] = $fieldType->name;
            }
        }

        $this->validate($request, $validationRules, $validationMessages, $validationAttributes);

        $ticket = new Ticket();
        $ticket->author_id = Auth::user()->steamid64;
        $ticket->ticket_type_id = $request->get('ticketType');
        $ticket->state = "open";
        $ticket->saveOrFail();

        foreach($request->toArray() as $fieldType => $value) {
            $ticketTypeFieldExplode = explode('-', $fieldType);

            if(count($ticketTypeFieldExplode) != 3)
                continue;

            $tf = TicketField::create(array(
                'author_id' => Auth::user()->steamid64,
                'ticket_id' => $ticket->id,
                'ticket_type_field_id' => $ticketTypeFieldExplode[2],
                'value' => $value
            ));

            $tf->saveOrFail();
        }

        $roleIDs = array();
        foreach($ticket->type->roles as $roleID) {
            $roleIDs[] = $roleID->role_id;
        }

        $admin_users = User::getUsersWithRoleIDs($roleIDs);

        foreach($admin_users as $admin_user) {
            $admin_user->sendDiscordMessage("Hey {$admin_user->personaname}! " . Auth::user()->personaname ." opened a new Ticket with type '{$ticket->type->name}'! View it on " . URL::to('/support/ticket/' . $ticket->id));
        }

        return redirect(URL::to('/support/ticket/' . $ticket->id))->with('status', 'You Successfully opened a new Ticket!');
    }

    public function ajaxSearchServerName(Request $request) {
        return DB::table("servers")->where('name', 'LIKE', '%' . $request->get('query') . '%')->orderBy('name', 'ASC')->select('id', 'name as text')->paginate(10); //  WHERE name LIKE ? GROUP BY steamid ORDER BY last_used DESC LIMIT 10", ['%' . $request->get('query') . '%']);
    }

    public function viewTicket(Ticket $ticket) {
        if(!$this->checkTicketAccess($ticket))
            abort(404);
        $ticketAdmin = $this->checkTicketAccess($ticket,true);
        $ticketClosedBy = User::whereSteamid64($ticket->closed_by)->first();
        return view('support.ticket.view', compact('ticket', 'ticketAdmin', 'ticketClosedBy'));
    }

    public function replyTicket(Ticket $ticket, Request $request) {
        if(Auth::user()->can('ban_ticket_system'))
            return back('You do not have permissions to create a new ticket!');
        if($request->get('action') === 'close_ticket' && $ticket->state !== "closed" && $this->checkTicketAccess($ticket, true)) {
            $ticket->state = "closed";
            $ticket->closed_by = Auth::user()->steamid64;
            $ticket->save();
            $closer = Auth::user()->personaname;
            $ticket->author->sendDiscordMessage("Your ticket has been closed by '{$closer}'! View it at " . URL::to('/support/ticket/' . $ticket->id));
            return back()->with('status', 'Successfully closed the ticket!');
        } elseif ($this->checkTicketAccess($ticket)) {
            TicketMessage::create(array(
                'author_id' => Auth::user()->steamid64,
                'ticket_id' => $ticket->id,
                'message' => $request->get('support-message')
            ))->saveOrFail();

            if($ticket->state === "closed") {
                $ticket->state = "open";
                $ticket->save();
            }

            if(Auth::user()->steamid64 !== $ticket->author->steamid64) {
                $closer = Auth::user()->personaname;
                $ticket->author->sendDiscordMessage("{$closer} replied to your ticket! View it at " . URL::to('/support/ticket/' . $ticket->id));
            } else {
                $roleIDs = array();
                foreach($ticket->type->roles as $roleID) {
                    $roleIDs[] = $roleID->role_id;
                }

                $admin_users = User::getUsersWithRoleIDs($roleIDs);

                $closer = Auth::user()->personaname;

                foreach($admin_users as $admin_user) {
                    $admin_user->sendDiscordMessage("{$closer} replied to a ticket, which you can process! View it at " . URL::to('/support/ticket/' . $ticket->id));
                }
            }
        }
        return back();
    }
}
