<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocCountry extends Model
{
    protected $fillable = [
        'name', 'coordinates', 'accuracy_level', 'country_id'
    ];

    public function states() {
        return $this->hasMany('App\LocState', 'country_id', 'id');
    }
}
