<?php

return [

    /*
     * Redirect URL after login
     */
    'redirect_url' => env('APP_URL') . '/login',
    /*
     * API Key (http://steamcommunity.com/dev/apikey)
     */
    'api_key' => env('WEB_API', null),
    /*
     * Is using https ?
     */
    'https' => substr_compare(env('APP_URL'), 'https', 0, 5) === 0
];
