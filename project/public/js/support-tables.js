jQuery(function() {
    Handlebars.registerHelper('log', function(x) {
        console.log(x);
        return "";
    });

    var tableBonesTemplate = Handlebars.compile($("#support-table-bones-template").html());
    var tableRowsTemplate = Handlebars.compile($("#support-table-body-template").html());
    var tableFooterTemplate = Handlebars.compile($("#support-table-footer-template").html());
    var latestData = {};

    var ticketTypes = {};

    var currentPage = 1;

    $.get("/support/ticketTypes", function(types) {
        ticketTypes = types;
        $("#support-table").html(tableBonesTemplate({types: ticketTypes}));

        $('[data-filter-type]').each(function(k, v) {
            $(v).on('click', function() {
                updateTable(false);
            });
        });

        updateTable(false);
    });

    function getSelectedTypes() {
        var ids = [];

        $('[data-filter-type="ticketType"]').each(function(k, checkbox) {
            checkbox = $(checkbox)[0];
            if(checkbox.checked)
                ids.push(+checkbox.dataset.filterId);
        });

        return ids;
    }

    function getSelectedStates() {
        var states = [];

        $('[data-filter-type="state"]').each(function(k, checkbox) {
            checkbox = $(checkbox)[0];
            if(checkbox.checked)
                states.push(checkbox.dataset.filterId);
        });

        return states;
    }

    updateTableReq = null;

    function updateTable(pagination) {
        if(!pagination) {
            page = 1;
        }

        if(updateTableReq)
            updateTableReq.abort();

        updateTableReq = $.get("/support/list", {types: getSelectedTypes(), states: getSelectedStates(), page: page}, function(a) {
            updateTableReq = null;
            var footer = {
                current_page: a.current_page,
                from: a.from,
                last_page: a.last_page,
                per_page: a.per_page,
                to: a.to,
                total: a.total
            };

            $("#support-table-footer").html(tableFooterTemplate(footer));
            $("#support-table-body").html(tableRowsTemplate(a.data));

            if(page === 1)
                $("#support-previous-page").prop('disabled', true);
            else
                $("#support-previous-page").prop('disabled', false);

            if(page === a.last_page)
                $("#support-next-page").prop('disabled', true);
            else
                $("#support-next-page").prop('disabled', false);

            $("#support-previous-page").on('click', function() {
                if(page > 1) {
                    page--;
                    updateTable(true);
                } else {
                    $("#support-previous-page").prop('disabled', true);
                }
            });

            $("#support-next-page").on('click', function() {
                if(page < a.last_page) {
                    page++;
                    updateTable(true);
                } else {
                    $("#support-next-page").prop('disabled', true);
                }
            });
        })
    }
});
