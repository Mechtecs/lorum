<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <script src="/js/jquery-3.1.1.js"></script>
        @yield("header")
        <title>@yield("title") - @setting("title")</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/app.css?t={{\Carbon\Carbon::now()->timestamp}}">
        <link rel="stylesheet" href="/css/jquery-ui.min.css">
        <link rel="stylesheet" href="/css/jquery-ui.structure.min.css">
        <link rel="stylesheet" href="/css/jquery-ui.theme.min.css">
        <link rel="stylesheet" href="/css/random.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
        <script src="/js/bootstrap.js"></script>
        <script src="/js/handlebars-v4.0.5.js"></script>
        <script src="/js/typeahead.bundle.js"></script>
        <script src="/js/jquery-ui.js"></script>
        <script src="/ckeditor/ckeditor.js"></script>
        @setting("header")
    </head>
    <body class="random-background">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{URL::to('/')}}">
                        <span>
                            @setting("title")
                        </span>
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li @if(Request::is('servers*')) class="active" @endif><a href="{{URL::to('/servers')}}">Servers</a></li>
                        <li @if(Request::is('forum*')) class="active" @endif><a href="{{URL::to('/forum')}}">Forum</a></li>
                        <li @if(Request::is('team*')) class="active" @endif><a href="{{URL::to('/team')}}">Team</a></li>
                        <li @if(Request::is('bans*')) class="active" @endif><a href=" @setting("banUrl") ">Bans</a></li>
                        <li @if(Request::is('join*')) class="active" @endif><a href="{{URL::to('/join')}}">Join Us! @if(!Auth::guest())
                                    <?php
                                        global $unread;
                                        $unread = 0;
                                        if(Auth::user()->can("general_admin") || Auth::user()->can("join_admin")) {
                                            \App\Models\Application::chunk(25, function($applications) {
                                                foreach($applications as $application) {
                                                    if(!$application->allMessagesRead()) {
                                                        global $unread;
                                                        $unread++;
                                                    }
                                                }
                                            });
                                        } else {
                                            foreach(\App\Models\Application::where("author_id", "=", Auth::user()->steamid64)->get() as $application) {
                                                if(!$application->allMessagesRead()) {
                                                    global $unread;
                                                    $unread++;
                                                }
                                            }
                                        }
                                        global $unread;
                                        print "<span class='badge'>" . $unread . "</span>";
                                ?> @endif </a></li>
                        <li @if(Request::is('rules*')) class="active" @endif><a href="{{URL::to('/rules/server')}}">Rules</a></li>
                        <li @if(Request::is('support*')) class="active" @endif><a href="{{URL::to('/support')}}">Support @if(!(\Auth::guest())) <span class='badge'>{{\App\Ticket::where('state', '=', 'open')->accessibleTickets()->count()}}</span> @endif </a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right fake-navbar-right">
                        @if(Auth::guest())
                            <li>
                                <a href="/login">Login</a>
                            </li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->personaname }} <span class="caret" /></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{URL::to('/profile/' . Auth::user()->steamid64)}}">Profile</a></li>
                                    <li><a href="{{URL::to('/profile/settings')}}">Settings</a></li>
                                    @if(Auth::user()->can(array('general_admin', 'general_moderator')))<li><a href="{{URL::to('/admin')}}">Admin Panel</a></li>@endif
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/logout">Logout</a></li>
                                </ul>
                            </li>
                        @endif
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="ts3server://eu.3kliksphilip.com">Join TS3</a></li>
                        <li>
                            <a href="https://discord.gg/6hGy9n6" target="_blank">
                                <img src="https://3kliksphilip.com/discord-logo.png" height="21px">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container" style="margin-top: 96px!important;">
            @if(!Auth::guest() && !(Auth::user()->discord_link))
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Please <a href="{{URL::to('/link/discord')}}" style="font-weight: bolder; text-decoration: underline!important;">link</a> your discord profile to get notifications about your threads and application!
                </div>
            @endif
            @yield("container")
        </div>
        <hr>
        <div class="footer">
            <p>This software is a product of the @setting("title") - Contribute on <a href=" @setting("projectUrl") "> @setting("projectUrlName") </a>. &copy; 2017</p>
            <p> @setting("sponsor") </p>
            <p> @setting("privacyPolicy") </p>
        </div>
    </body>
</html>
