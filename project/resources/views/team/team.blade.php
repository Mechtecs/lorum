@extends("app.layout")

@section("title", "Team List")

@section("container")
    <div class="transparent-background">
        @foreach($roles as $role)
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size: 1.25em;">
                    {{$role->display_name}}
                </div>
                <table class="table">
                    @foreach($role->users as $user)
                        <tr>
                            <td width="10%"><a href="{{URL::to('/profile/' . $user->steamid64)}}"><img src="{{$user->avatarmedium}}" /></a></td>
                            <td style="font-size: 1.05em;">
                                {!! $user->personaname() !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endforeach
    </div>
@endsection
