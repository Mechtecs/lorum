<?php global $parser; ?>
@extends("support.layout")

@section("content")
    <h2>{{$ticketType->name}}</h2>
    <h4 class="well">{!! $parser->except('youtube')->parse($ticketType->description) !!}</h4>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! $ticketType->render($_GET['ticketType']) !!}
@endsection
