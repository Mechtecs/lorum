<?php global $parser; ?>
@extends("support.layout")

@section("content")
    <h2>'{{$ticket->type->name}}' created by {!! $ticket->author->personaname() !!}</h2>
    <h4 class="well">{!! $parser->except('youtube')->parse($ticket->type->description) !!}</h4>

    @if($ticket->state === "closed" && $ticketClosedBy)
        <h4>Closed by {!! $ticketClosedBy->personaname() !!}</h4>
    @endif

    <br />

    @foreach($ticket->fields as $field)
        {!! $field->render($ticket) !!}
    @endforeach
    @foreach($ticket->additionalFields() as $field)
        {!! $field->render(true, $field->description, $ticket) !!}
    @endforeach

    <br />

    <h4>Messages</h4>
    @foreach($ticket->messages as $message)
        <div class="panel panel-default">
            <div class="panel-heading">
                {!! $message->author->personaname() !!}
            </div>
            <div class="panel-body">
                {!! $parser->except('youtube')->parse(htmlspecialchars($message->message)) !!}
            </div>
            <div class="panel-footer">
                {{$message->created_at}}
            </div>
        </div>
    @endforeach
    @if($ticket->messages()->count() === 0)
        <h5>
        No activity yet :(
        </h5>
    @endif
    <form novalidate method="post" action="{{URL::to('/support/ticket/' . $ticket->id)}}">
        <textarea id="support-message" name="support-message"></textarea>
        <input type="submit" class="btn btn-success" value="Send Message!"/>
    </form>
    @if($ticketAdmin && $ticket->state !== "closed")
        <form novalidate method="post" action="{{URL::to('/support/ticket/' . $ticket->id)}}">
            <input type="hidden" name="action" value="close_ticket">
            <input type="submit" class="btn btn-warning" value="Close Ticket">
        </form>
    @endif
    <BR />
    <script>
        CKEDITOR.replace('support-message');
    </script>
@endsection
