@if($view && $data)
    <div class="form-group">
        <label for="field-{{$field->type}}-{{$field->id}}">{{$field->name}}</label>
        <p class="well" id="field-{{$field->type}}-{{$field->id}}">
            {{$data}}
        </p>
    </div>
@else
    <div class="form-group">
        <label for="field-{{$field->type}}-{{$field->id}}">{{$field->name}}</label>
        @if($field->description)
            <div class="well well-sm">
                {{$field->description}}
            </div>
        @endif
        <input type="password" class="form-control" id="field-{{$field->type}}-{{$field->id}}" name="field-{{$field->type}}-{{$field->id}}" value="{{old("field-{$field->type}-{$field->id}")}}" required />
    </div>
@endif
