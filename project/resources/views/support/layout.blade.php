@extends("app.layout")

@section("title", "Support")

@section("container")
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if(isset($links))
        {!! $links->links() !!}
    @endif
    <div class="transparent-background">
        @yield("content")
    </div>
    @if(isset($links))
        {!! $links->links() !!}
    @endif
@endsection
