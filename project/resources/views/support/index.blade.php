@extends("support.layout")

@section("header")
    <script src="{{URL::to('/js/support-tables.js')}}"></script>
@endsection

@section("content")
    <div class="transparent-background">
        <noscript>
            {{$tickets->links()}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tickets
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Type</td>
                        <td>State</td>
                        <td>Author</td>
                        <td>Date</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tickets as $ticket)
                        <tr class="ticket-type-{{$ticket->type->id}}-{{$ticket->state}}">
                            <td><a href="{{URL::to('/support/ticket/' . $ticket->id)}}">{{$ticket->id}}</a></td>
                            <td><a href="{{URL::to('/support/ticket/' . $ticket->id)}}">{{$ticket->type->name}}</a></td>
                            <td>{{$ticket->state}}</td>
                            <td>{!!$ticket->author->personaname()!!}</td>
                            <td>{{$ticket->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </noscript>
        <div id="support-table">

        </div>

        @include("support.indexHandlebars")

        <form novalidate method="get" action="{{URL::to('/support/ticket/new')}}">
            <div class="input-group">
                <select id="ticket-type" name="ticketType" class="form-control">
                    @foreach($ticketTypes as $ticketType)
                        <option value="{{$ticketType->id}}">{{$ticketType->name}}</option>
                    @endforeach
                </select>
                <span class="input-group-btn">
                <button type="submit" value="New Ticket!" class="btn btn-success">New Ticket!</button>
            </span>
            </div>
        </form>
    </div>
@endsection
