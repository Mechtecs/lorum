@extends("admin.layout")

@section("content")
    <form action="{{URL::to("/admin/ticket/type/{$ticketType->id}/field/new")}}" method="post">
        <div class="form-group">
            <label for="name">Type Field Name:</label>
            <input type="text" name="name" id="name" placeholder="Ban Disputes" class="form-control" value="{{old("name")}}" required />
        </div>
        <div class="form-group">
            <label for="description">Type Field Description:</label>
            <textarea name="description" id="description" rows="10" cols="80">
                {{old("description")}}
            </textarea>
        </div>
        <div class="form-group">
            <label for="type">Type</label>
            <select name="type" id="type" class="form-control" required>
                @foreach(array("string", "textarea", "integer", "date", "password", "steamid", "serverid", "format_view") as $item)
                    <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="order">Order</label>
            <input type="number" name="order" id="order" placeholder="42" class="form-control" value="{{old("order")}}" required />
        </div>
        <input type="submit" class="btn btn-success" />
    </form>

    <script>
        jQuery(function() {
            CKEDITOR.replace("description");
        });
    </script>
@endsection
