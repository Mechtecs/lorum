@extends("admin.layout")

@section("content")
    <form action="{{URL::to("/admin/server-list")}}" method="post">
        <h2>Ticket System:</h2>

        <BR />

        <table class="table table-responsive">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody>
                @foreach($ticketTypes as $ticketType)
                    <tr>
                        <td>{{$ticketType->id}}</td>
                        <td>{{$ticketType->name}}</td>
                        <td>
                            <a href="{{URL::to('/admin/ticket/type/' . $ticketType->id . '/edit')}}" class="btn btn-warning">Edit</a>
                            <a href="{{URL::to('/admin/ticket/type/' . $ticketType->id . '/delete')}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <a href="{{URL::to("/admin/ticket/type/new")}}" class="btn btn-warning btn-sm">Add Ticket Type</a>
    </form>
@endsection
