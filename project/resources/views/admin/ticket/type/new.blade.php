@extends("admin.layout")

@section("content")
    <form action="{{URL::to("/admin/ticket/type/new")}}" method="post">
        <div class="form-group">
            <label for="name">Type Name:</label>
            <input type="text" name="name" id="name" placeholder="Ban Disputes" class="form-control" value="{{old("description")}}" required />
        </div>
        <div class="form-group">
            <label for="description">Type Description:</label>
            <textarea name="description" id="description" rows="10" cols="80">
                {{old("description")}}
            </textarea>
        </div>
        <div class="form-group">
            <label for="order">Order</label>
            <input type="number" name="order" id="order" placeholder="42" class="form-control" value="{{old("order")}}" required />
        </div>
        <input type="submit" class="btn btn-success" />
    </form>

    <script>
        jQuery(function() {
            CKEDITOR.replace("description");
        });
    </script>
@endsection
