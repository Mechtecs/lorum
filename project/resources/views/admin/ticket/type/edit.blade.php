@extends("admin.layout")

@section("content")
    <h4>General Information:</h4>
    <form action="{{URL::to("/admin/ticket/type/{$ticketType->id}/edit")}}" method="post">
        <div class="form-group">
            <label for="disabled">Disabled (Set to disable the creation of the ticket type):</label>
            <input type="checkbox" name="disabled" id="disabled" class="form-control" @if($ticketType->disabled) checked="checked" @endif />
        </div>
        <div class="form-group">
            <label for="name">Type Name:</label>
            <input type="text" name="name" id="name" placeholder="Ban Disputes" class="form-control" value="{{$ticketType->name}}" required />
        </div>
        <div class="form-group">
            <label for="description">Type Description:</label>
            <textarea name="description" id="description" rows="10" cols="80">
                {{$ticketType->description}}
            </textarea>
        </div>
        <div class="form-group">
            <label for="order">Order</label>
            <input type="number" name="order" id="order" placeholder="42" class="form-control" value="{{$ticketType->order}}" required />
        </div>

        <h4>Roles which can process the tickets:</h4>
        <dl class="type-edit-dl">
            @foreach($roles as $role)
                <dt><input type="checkbox" name="role[{{$role->id}}]" @if($ticketType->hasRole($role->id)) checked @endif/></dt>
                <dd>{{$role->display_name}}</dd>
            @endforeach
        </dl>

        <h4>
            Fields:
        </h4>

        <table class="table">
            <thead>
                <tr>
                    <td>Id</td>
                    <td>Name</td>
                    <td>Type</td>
                    <td>Order</td>
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody>
                @foreach($fields as $field)
                    <tr>
                        <td>{{$field->id}}</td>
                        <td>{{$field->name}}</td>
                        <td>{{$field->type}}</td>
                        <td>{{$field->order}}</td>
                        <td>
                            <a href="{{URL::to("/admin/ticket/type/{$ticketType->id}/field/{$field->id}/edit")}}" class="btn btn-warning">Edit</a>
                            <a href="{{URL::to("/admin/ticket/type/{$ticketType->id}/field/{$field->id}/delete")}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="form-group">
            <label for="color_open">Color for open ticket (white is ignored):</label>
            <input type="color" name="color_open" id="color_open" class="form-control" value="{{$ticketType->color_open}}" required/>
        </div>

        <div class="form-group">
            <label for="color_closed">Color for closed ticket (white is ignored):</label>
            <input type="color" name="color_closed" id="color_closed" class="form-control" value="{{$ticketType->color_closed}}" required/>
        </div>

        <a href="{{URL::to("/admin/ticket/type/{$ticketType->id}/field/new")}}" class="btn btn-info">New Ticket Field</a>
        <a class="btn btn-success" onclick="renderForm({{$ticketType->id}})">Render Form Preview</a>
        <input type="submit" class="btn btn-success" />
    </form>

    <div id="render-preview-target" style="display: none;">

    </div>

    <script>
        jQuery(function() {
            CKEDITOR.replace("description");
        });

        function renderForm(ticketTypeId) {
            $("#render-preview-target").css("display", "block");
            jQuery.get("{{URL::to("/api/forms/render/{$ticketType->id}")}}", function(data) {
                $("#render-preview-target").html(data);
                $("#render-preview-target").find('script').each(function(index) {
                    eval($(this).innerHTML);
                });
            });
        }
    </script>
@endsection
