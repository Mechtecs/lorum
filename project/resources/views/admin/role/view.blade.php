@extends("admin.layout")

@section("content")
    <div>
        <form action="{{URL::to('/admin/roles/view/' . $role->id)}}" method="post">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Miscellanous Information
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{$role->name}}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="display_name">Display Name:</label>
                        <input type="text" class="form-control" id="display_name" name="display_name" value="{{$role->display_name}}">
                    </div>
                    <div class="form-group">
                        <label for="username_style">Username Style:</label>
                        <input type="text" class="form-control" id="username_style" name="username_style" value="{{$role->username_style}}">
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <input type="text" class="form-control" id="description" name="description" value="{{$role->description}}">
                    </div>
                </div>
                <div class="panel-footer">
                    <input type="submit" class="btn btn-success" value="Save">
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Permissions
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <td>
                                Name:
                            </td>
                            <td>
                                Description:
                            </td>
                            <td>
                                Set?
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($role->allPerms() as $permission)
                            <tr>
                                <td>
                                    {{$permission->name}}
                                </td>
                                <td>
                                    {{$permission->description}}
                                </td>
                                <td>
                                    <input type="checkbox" name="permission[{{$permission->id}}]" {{$permission->available ? 'checked' : ''}}>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="panel-footer">
                    <input type="submit" class="btn btn-success" value="Save">
                </div>
            </div>
        </form>
    </div>
@endsection