@extends("app.layout")

@section("title", "AdminCP")

@section("container")
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-2">
            <div class="list-group">
                <a href="{{URL::to('/admin')}}" class="list-group-item {{(Request::is('admin')) ? 'active' : ''}}">
                    Dashboard
                </a>
                <a href="{{URL::to('/admin/users')}}" class="list-group-item {{(Request::is('admin/users*')) ? 'active' : ''}}">
                    Users
                </a>
                @permission("general_admin")
                    <a href="{{URL::to('/admin/roles')}}" class="list-group-item {{(Request::is('admin/roles*')) ? 'active' : ''}}">
                        Roles
                    </a>
                    <a href="{{URL::to('/admin/forums')}}" class="list-group-item {{(Request::is('admin/forums*')) ? 'active' : ''}}">
                        Forums
                    </a>
                    <a href="{{URL::to('/admin/server-list')}}" class="list-group-item {{(Request::is('admin/server-list*')) ? 'active' : ''}}">
                        Server List
                    </a>
                    <a href="{{URL::to('/admin/ticket')}}" class="list-group-item {{(Request::is('admin/ticket*')) ? 'active' : ''}}">
                        Ticket System
                    </a>
                @endpermission
            </div>
        </div>
        <div class="col-md-9 col-md-offset-1 transparent-background">
            @yield("content")
        </div>
    </div>
@endsection
