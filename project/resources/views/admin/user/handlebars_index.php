<script id="table-more-template" type="text/x-handlebars-template">
    {{#each users}}
        <tr>
            <td><img src="{{avatar}}" class="img-circle"></td>
            <td>{{personaname}}</td>
            <td>
                <a class="btn btn-info" href="/admin/users/view/{{steamid64}}">View</a>
                <a class="btn btn-warning" href="/admin/users/edit/{{steamid64}}">Edit</a>
            </td>
        </tr>
    {{/each}}
</script>

<script id="suggestion-template" type="text/x-handlebar-template">
    {{this}}
</script>