@extends("admin.layout")

@section("content")
    <div>
        <div class="col-md-3">
            <div>
                <p>{!! $user->personaname() !!}</p>
                <img src="{{$user->avatarfull}}" class="img-rounded">
            </div>
            @permission("general_admin")
                <div style="margin-top: 1em;">
                    <a href="{{URL::to('/admin/users/edit/' . $user->steamid64)}}" class="btn btn-warning">Edit</a>
                </div>
            @endpermission
            <div style="margin-top: 0.5em;">
                <form method="post" action="{{URL::to('/admin/users/view/' . $user->steamid64 . '/ban')}}">
                    <input type="submit" class="btn btn-danger" value="Ban">
                    <input type="datetime" id="until" name="until" id="until" style="margin-top: 0.5em;" value="{{old("until")}}">
                </form>
            </div>
        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Threads started by this person
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Topic</td>
                            <td>Answers</td>
                            <td>Views</td>
                            <td>Date</td>
                            <td>Actions</td>
                        </tr>
                    </thead>
                    @foreach($user->threads()->orderBy('created_at', 'desc') as $thread)
                        <tr>
                            <td>{{$thread->id}}</td>
                            <td>{{$thread->title}}</td>
                            <td>{{$thread->posts()->count()}}</td>
                            <td>... Views</td>
                            <td>{{$thread->created_at}}</td>
                            <td>

                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Posts written by this person
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Thread</td>
                            <td>Date</td>
                            <td>Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user->posts()->orderBy('created_at', 'desc') as $post)
                            <tr>
                                <td>{{$post->id}}</td>
                                <td>{{$post->thread->title}}</td>
                                <td>{{$post->created_at}}</td>
                                <td>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $("#until").datepicker({ minDate: 1, maxDate: "+1Y"});
        });
    </script>
@endsection
