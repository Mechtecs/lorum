@extends("admin.layout")

@section("content")
    <div>
        <form action="{{URL::to('/admin/users/edit/' . $user->steamid64)}}" method="post">
            <table class="table table-default">
                <tbody>
                    <tr>
                        <td>
                            Name:
                        </td>
                        <td>
                            {!! $user->personaname() !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Real Name (on steam):
                        </td>
                        <td>
                            {{$user->realname}}
                        </td>
                    </tr>
                    @if(isset($location))
                        <tr>
                            <td>
                                Location:
                            </td>
                            <td>
                                {{$location[0]->country_name}}, {{$location[0]->state_name}}, {{$location[0]->city_name}}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td>Role:</td>
                        <td>
                            <select name="role">
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}" @if($user->roles()->first()->id === $role->id) selected @endif >{{$role->display_name}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
@endsection
