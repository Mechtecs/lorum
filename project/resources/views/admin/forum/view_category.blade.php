@extends("admin.layout")

@section("content")
    <form action="{{URL::to('/admin/forums/category/edit/' . $category->id)}}" method="post">
        <div class="panel panel-default">
            <div class="panel-heading">Category Information</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" id="title" name="title" class="form-control" value="{{$category->title}}">
                </div>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <input type="text" id="description" name="description" class="form-control" value="{{$category->description}}">
                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" value="Save Changes" class="btn btn-success">
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Subcategories</div>
            <table class="table">
                @foreach($category->subcategories as $subcategory)
                    <tr>
                        <td>
                            <a href="{{URL::to('/admin/forums/subcategory/edit/' . $subcategory->id)}}">{{$subcategory->title}}</a>
                        </td>
                        <td>
                            Order: <input type="number" name="subcategory_order[{{$subcategory->id}}]" value="{{$subcategory->order}}" style="margin-left: 0.25em; width: 2.5em;" min="0" max="99">
                        </td>
                        <td>
                            <a href="{{URL::to('/admin/forums/subcategory/delete/' . $subcategory->id)}}" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </table>
            <div class="panel-footer">
                <input type="submit" value="Save Changes" class="btn btn-success">
            </div>
        </div>
    </form>
@endsection