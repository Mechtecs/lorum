@extends("admin.layout")

@section("content")
    <form action="{{URL::to('/admin/forums')}}" method="post">
        <h2>Categories:</h2>
            @foreach($categories as $category)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{$category->title}}
                    </div>
                    <table class="table">
                        @foreach($category->subcategories as $subcategory)
                            <tr>
                                <td style="width: 65%;">
                                    <span>{{$subcategory->title}}</span>
                                    <div class="text-info" style="font-size: 0.75em;">{{$subcategory->description}}</div>
                                </td>
                                <td style="width: 10%;">
                                    <span><span class="glyphicon glyphicon-list-alt"></span> {{$subcategory->threads()->count()}}</span><BR>
                                    <span><span class="glyphicon glyphicon-comment"></span> -</span>
                                </td>
                                <td style="width: 15%;">
                                    @if(is_null($subcategory->latestThread()))
                                        <span>No threads yet :(</span>
                                    @else
                                        <span>{{$subcategory->latestThread()->title}}</span><BR>
                                        <span class="small-text">by {!! $subcategory->latestThread()->author->personaname() !!}</span><BR>
                                    @endif
                                </td>
                                <td style="width: 10%;">
                                    <a href="{{URL::to('/admin/forums/subcategory/edit/' . $subcategory->id)}}" class="btn btn-warning">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="panel-footer">
                        <a href="{{URL::to('/admin/forums/category/edit/' . $category->id)}}" class="btn btn-info">View / Edit Category</a>
                        <a href="{{URL::to('/admin/forums/subcategory/create/' . $category->id)}}" class="btn btn-info">Create Subcategory</a>
                        <a href="{{URL::to('/admin/forums/category/delete/' . $category->id)}}" class="btn btn-danger">Delete Category</a>
                        <span>Order: <input type="number" name="order[category][{{$category->id}}]" min="0" max="99" style="width: 2.5em;" value="{{$category->order}}"></span>
                    </div>
                </div>
            @endforeach
        <a href="{{URL::to('/admin/forums/category/create')}}" class="btn btn-info">Create Category</a>
        <input type="submit" class="btn btn-success" value="Update Order">
    </form>
@endsection
