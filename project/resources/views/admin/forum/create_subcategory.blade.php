@extends("admin.layout")

@section("content")
    <form action="{{URL::to('/admin/forums/subcategory/create/' . $id)}}" method="post">
        <h2>Create SubCategory:</h2>
        <div class="panel panel-default">
            <div class="panel-heading">
                SubCategory Info
            </div>
            <div class="panel-body">
                <div class="form-group-sm">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" name="title" id="title">
                </div>
                <div class="form-group-sm">
                    <label for="description">Description:</label>
                    <input type="text" class="form-control" name="description" id="description">
                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" value="Create SubCategory" class="btn btn-success">
            </div>
        </div>
    </form>
@endsection
