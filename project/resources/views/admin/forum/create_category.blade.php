@extends("admin.layout")

@section("content")
    <form action="{{URL::to('/admin/forums/category/create')}}" method="post">
        <h2>Create Category:</h2>
        <div class="panel panel-default">
            <div class="panel-heading">
                Category Info
            </div>
            <div class="panel-body">
                <div class="form-group-sm">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" name="title" id="title">
                </div>
                <div class="form-group-sm">
                    <label for="description">Description:</label>
                    <input type="text" class="form-control" name="description" id="description">
                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" value="Create Category" class="btn btn-success">
            </div>
        </div>
    </form>
@endsection