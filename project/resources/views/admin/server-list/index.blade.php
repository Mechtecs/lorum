@extends("admin.layout")

@section("content")
    <form action="{{URL::to("/admin/server-list")}}" method="post">
        <h2>Server List:</h2>
        @foreach($servergroups as $servergroup)
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{$servergroup->name}} - Enabled: <input type="checkbox" id="servergroup_enabled[{{$servergroup->id}}]" name="servergroup_enabled[{{$servergroup->id}}]" @if($servergroup->active) checked="checked" @endif> - Order: <input type="number" id="servergroup_order[{{$servergroup->id}}]" name="servergroup_order[{{$servergroup->id}}]" value="{{$servergroup->order}}" required />
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <td>Name</td>
                        <td>IP</td>
                        <td>Port</td>
                        <td>Order</td>
                        <td>Enabled</td>
                        <td>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($servergroup->servers()->orderBy("order", "asc")->get() as $server)
                        <tr>
                            <td>{{$server->name}}</td>
                            <td>{{$server->ip}}</td>
                            <td>{{$server->port}}</td>
                            <td><input type="number" id="server_orders[{{$server->id}}]" name="server_orders[{{$server->id}}]" value="{{$server->order}}" /></td>
                            <td><input type="checkbox" id="server_enabled[{{$server->id}}]" name="server_enabled[{{$server->id}}]" @if($server->active) checked="checked" @endif></td>
                            <td>
                                <a href="{{URL::to("/admin/server-list/server/" . $server->id)}}" class="btn btn-warning btn-sm">Edit</a>
                                <a href="{{URL::to("/admin/server-list/server/" . $server->id . "/delete")}}" class="btn btn-danger btn-sm">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="panel-footer">
                    <a href="{{URL::to("/admin/server-list/servergroup/" . $servergroup->id)}}" class="btn btn-warning btn-sm">Edit Server Group</a>
                    <a href="{{URL::to("/admin/server-list/servergroup/" . $servergroup->id . "/delete")}}" class="btn btn-danger btn-sm">Delete Server Group</a>
                </div>
            </div>
        @endforeach
        <BR />
        <a href="{{URL::to("/admin/server-list/server")}}" class="btn btn-warning btn-sm">Add Server</a>
        <a href="{{URL::to("/admin/server-list/servergroup")}}" class="btn btn-warning btn-sm">Add Server Group</a>
        <input type="submit" class="btn btn-success btn-sm" value="Update" />
    </form>
@endsection
