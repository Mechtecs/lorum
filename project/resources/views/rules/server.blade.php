@extends("app.layout")

@section("title", "Rules")

@section("container")
    <div class="transparent-background" style="padding: 24px 24px 24px 24px;">
        <span style="font-size: large;" original-title="">1.</span><span style="font-size: x-small;" original-title=""><span style="font-family: Times New Roman;" original-title="">&nbsp;</span></span><span style="font-size: large;" original-title=""><span style="color: #ff3333;" original-title="">No cheats and exploits</span></span><span style="font-size: large;" original-title=""> (unless showing them off&nbsp;to mappers on playtesting servers) or 3rd party tools that give you </span><span style="font-size: large;" original-title="">an unfair advantage over other players</span><span style="font-size: large;" original-title="">.</span><br>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: medium;" original-title=""><span style="font-size: large;" original-title="">2.&nbsp;<span style="color: #ff3333;" original-title="">Spam or&nbsp;advertising will be punished</span> with a temporary gag/mute or ban if serious. If the offense is repeated the player will be given a temporary ban.</span></span></div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">3.</span> <span style="font-size: large;" original-title=""><span style="color: #ff3333;" original-title="">Be respectful to other players</span>:&nbsp;</span><span style="font-size: large;" original-title="">racism</span><span style="font-size: large;" original-title="">,</span> <span style="font-size: large;" original-title="">prejudice</span><span style="font-size: large;" original-title="">, </span><span style="font-size: large;" original-title="">political and religious comments aren’t welcome if hostile in nature</span><span style="font-size: large;" original-title="">.</span><br>
        </div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">4.</span> <span style="color: #ff3333;" original-title=""><span style="font-size: large;" original-title="">Do </span><span style="font-size: large;" original-title="">not scream</span><span style="font-size: large;" original-title=""> or </span><span style="font-size: large;" original-title="">play music</span></span><span style="font-size: large;" original-title=""><span style="color: #ff3333;" original-title=""> in the voicechat</span>. </span><span style="font-size: large;" original-title="">No useless spamming of the mic</span><span style="font-size: large;" original-title="">. That means no songs/soundbytes, especially if fellow players are complaining.</span><br>
        </div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">5.</span><span style="font-size: x-small;" original-title=""><span style="font-family: Times New Roman;" original-title="">&nbsp;</span></span><span style="font-size: large;" original-title=""><span style="color: #ff3333;" original-title="">Do not&nbsp;grief</span></span><span style="font-size: large;" original-title="">, don’t try to help the other team or do things detrimental to your team’s members. Play competitively, always go for the objective.</span></div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">6.</span> <span style="font-size: large;" original-title="">Don't stay&nbsp;on the server when AFK (away from keyboard).</span></div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">7.</span><span style="font-size: x-small;" original-title=""><span style="font-family: Times New Roman;" original-title="">&nbsp;</span></span><span style="font-size: large;" original-title=""><span style="color: #ff3333;" original-title="">No name/role impersonation</span> of ANYONE</span><span style="font-size: large;" original-title="">, especially </span><span style="font-size: large;" original-title="">staff members/mappers</span><span style="font-size: large;" original-title=""> or even </span><span style="font-size: large;" original-title="">YouTubers</span><span style="font-size: large;" original-title="">! We know who is who and in case </span><span style="font-size: large;" original-title="">we can check your identity</span><span style="font-size: large;" original-title=""> in 3clicks</span><span style="font-size: x-small;" original-title=""><span style="font-family: AR BERKLEY;" original-title="">philip</span></span><span style="font-size: large;" original-title=""> (admins and moderators&nbsp;have special tags on the scoreboard and chat).</span><br>
        </div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">8.</span> <span style="font-size: large;" original-title="">Please speak English to communicate in the voice&nbsp;chat.</span></div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">9.</span><span style="font-size: x-small;" original-title=""><span style="font-family: Times New Roman;" original-title="">&nbsp;</span></span><span style="font-size: large;" original-title=""><span style="color: #ff3333;" original-title="">It is&nbsp;forbidden to use loopholes in our rules</span></span><span style="font-size: large;" original-title=""><span style="color: #ff3333;" original-title="">.</span>&nbsp;The final decision to ban you or not goes to the admin if he or she&nbsp;thinks your behaviour was disruptive for the server.</span><br>
        </div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">10.</span><span style="font-size: x-small;" original-title=""><span style="font-family: Times New Roman;" original-title="">&nbsp;</span></span><span style="font-size: large;" original-title=""><span style="color: #ff3333;" original-title="">Do not add admins or mods</span></span><span style="font-size: large;" original-title="">, if you need to tell us something use the forum.&nbsp;</span><span style="font-size: large;" original-title="">Only add us in case of URGENT necessity</span><span style="font-size: large;" original-title=""> like crashed servers.</span><br>
        </div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">11.</span> <span style="font-size: large;" original-title=""><span style="color: #ff3333;" original-title="">Elo-system farming</span> will result in a </span><span style="font-size: large;" original-title="">temporary ban</span><span style="font-size: large;" original-title=""> and </span><span style="font-size: large;" original-title=""><span style="color: #ff3333;" original-title="">removal of all your Elo-points</span></span><span style="font-size: large;" original-title="">.</span><br>
        </div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">12.</span> <span style="font-size: large;" original-title="">Goading or trolling players into behaving inappropriately is forbidden.</span><br>
        </div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">13.</span> <span style="font-size: large;" original-title="">To appeal a ban,&nbsp;<span style="color: #ff3333;" original-title="">make&nbsp;a thread on our forum</span>. </span><span style="font-size: large;" original-title="">Other ways will be ignored and may even increase your ban</span><span style="font-size: large;" original-title="">.</span><br>
        </div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">14.</span><span style="font-size: x-small;" original-title=""><span style="font-family: Times New Roman;" original-title="">&nbsp;</span></span><span style="font-size: large;" original-title=""><span style="color: #ff3333;" original-title="">The abuse of "!calladmin" is strictly prohibited</span></span><span style="font-size: large;" original-title="">.</span><br>
        </div>
        <br>
        <div style="text-align: left;" original-title=""><span style="font-size: large;" original-title="">15.</span><span style="font-size: x-small;" original-title=""><span style="font-family: Times New Roman;" original-title="">&nbsp;</span></span><span style="font-size: large;" original-title="">It's <span style="color: #ff3333;" original-title="">forbidden</span> to have a <span style="color: #ff3333;" original-title="">nickname that contains offensive content</span>, <span style="color: #ff3333;" original-title="">cheat websites</span>, <span style="color: #ff3333;" original-title="">invalid names</span>,&nbsp;<span style="color: #ff3333;" original-title="">unsafe or inappropriate content</span></span><span style="font-size: large;" original-title="">.</span></div>
    </div>
@endsection
