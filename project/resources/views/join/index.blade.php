@extends("join.layout")

@section("content")
    <div class="post_body scaleimages transparent-background" id="pid_" original-title="">
        <span style="font-size: small;" original-title=""><span style="font-weight: bold;" original-title=""><span style="color: #ffffff;" original-title="">This page contains information about Moderator Applications. Read this page <i>BEFORE</i> posting an application. Applications that do not meet the requirements will instantly be rejected by the Head-Administrators.</span></span></span><br>
        <br>
        <span style="color: #ffffff;" original-title=""><span style="font-size: small;" original-title=""><span style="text-decoration: underline;" original-title=""><span style="font-family: Arial, Helvetica, Verdana, sans-serif;" original-title=""><span style="font-weight: bold;" original-title="">What are we looking for?</span></span></span></span></span><br>
        <span style="font-size: small;" original-title=""><span style="color: #ffffff;" original-title=""><span style="font-family: Arial, Helvetica, Verdana, sans-serif;" original-title="">A mature individual who has good communicative skills (Great understanding of the English language, any other languages are a plus.) The applicant should be familiar with administrator plugins (such as SM, Sourcebans, ...) and forum management. We are currently looking for Moderators who have their residence in North-America, but we scrutinise each application equally and give each applicant due consideration.</span></span></span><br>
        <br>
        <span style="color: #ffffff;" original-title=""><span style="font-size: small;" original-title=""><span style="text-decoration: underline;" original-title=""><span style="font-family: Arial, Helvetica, Verdana, sans-serif;" original-title=""><span style="font-weight: bold;" original-title="">What do we expect from you?</span></span></span></span></span><ul>
            <li><span style="color: #ffffff;" original-title="">Strong work ethic</span><br>
            </li>
            <li><span style="color: #ffffff;" original-title="">Conviction</span><br>
            </li>
            <li><span style="color: #ffffff;" original-title="">Respect to the community and the individual</span><br>
            </li>
            <li><span style="color: #ffffff;" original-title="">Experience (within server- and forum moderating)</span><br>
            </li>
            <li><span style="color: #ffffff;" original-title="">Strong reasoning skills</span><br>
            </li>
            <li><span style="color: #ffffff;" original-title="">Teamwork</span><br>
            </li>
            <li><span style="color: #ffffff;" original-title="">Discretion</span><br>
            </li></ul>
        <br>
        <span style="color: #ffffff;" original-title=""><span style="font-weight: bold;" original-title=""><span style="font-size: small;" original-title=""><span style="font-family: Arial, Helvetica, Verdana, sans-serif;" original-title=""><span style="text-decoration: underline;" original-title="">When&nbsp;can I re-apply?</span></span></span></span></span><br>
        <span style="color: #ffffff;" original-title=""><span style="font-size: small;" original-title=""><span style="font-family: Arial, Helvetica, Verdana, sans-serif;" original-title="">An applicant is allowed to create a new application after the first one has been declined after three (3) months.</span></span></span><br>
        <br>
        <span style="color: #ffffff;" original-title=""><span style="font-weight: bold;" original-title=""><span style="font-size: small;" original-title=""><span style="font-family: Arial, Helvetica, Verdana, sans-serif;" original-title="">Other users will not be able to view your application, only Community Managers and&nbsp;Head-Administrators will be able to review your application.</span></span></span></span><br>
    </div>
    <h3 align="center"><a href="@if(!Auth::guest() && Auth::user()->applications()->count() > 0)# @else {{URL::to('/join/now')}} @endif" class="btn btn-success" @if(!Auth::guest() && Auth::user()->applications()->count() > 0)disabled @endif>Apply Now!</a></h3>
@endsection
