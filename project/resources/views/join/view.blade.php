@extends("join.layout")

@section("header")
    <style>
        dl > dt {
            margin-bottom: 1em;
        }

        dl > dd {
            padding-left: 0.5em;
            margin-bottom: 1em;
            text-align: justify;
        }
    </style>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Application Details
                </div>
                <div class="panel-body">
                    <dl class="info-list">
                        <dt>Personaname:</dt>
                        <dd>{!! $application->author->personaname() !!}</dd>
                        <dt>Birthday:</dt>
                        <dd>{{$application->birthday}}</dd>
                        <dt>Location:</dt>
                        <dd>{{$application->location}}</dd>
                        <dt>Languages:</dt>
                        <dd>{{$application->languages}}</dd>
                        <dt>Personal Information:</dt>
                        <dd>{{$application->text1}}</dd>
                        <dt>Why do you want to become a Moderator?</dt>
                        <dd>{{$application->text2}}</dd>
                        <dt>How much time can you devote to Moderating per week?</dt>
                        <dd>{{$application->text3}}</dd>
                        <dt>What are your personal shortcomings or attributes you can improve on?</dt>
                        <dd>{{$application->text4}}</dd>
                        <dt>Why should we pick you over another applicant?</dt>
                        <dd>{{$application->text5}}</dd>
                        <dt>Do you have any prior experience in moderating servers or forums?</dt>
                        <dd>{{$application->text6}}</dd>
                        <dt>Do you have any experience with SourceMod, SourceBans, Discord or other skills that could be helpful like coding?</dt>
                        <dd>{{$application->text7}}</dd>
                    </dl>
                </div>
            </div>
            @permission(['general_admin', 'join_admin'])
                <div class="panel panel-warning">
                    <div class="panel-heading">Voting System</div>
                    <div class="panel-default">
                        <span class="text-success">Yes</span>: {{$application->votesFor('yes')}}<BR>
                        <span class="text-danger">No</span>: {{$application->votesFor('no')}}<BR>
                        <span class="text-warning">Meh</span>: {{$application->votesFor('meh')}}<BR>
                        <input type="button" onclick="vote('yes')" value="Yes" class="btn btn-success btn-sm">
                        <input type="button" onclick="vote('no')" value="No" class="btn btn-danger btn-sm">
                        <input type="button" onclick="vote('meh')" value="Meh" class="btn btn-warning btn-sm">
                    </div>
                </div>
            @endpermission
        </div>
        <div class="col-md-6">
            <h2>Responses:</h2>
            @foreach($application->responses as $message)
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            @if($message->type === "regular")
                                <b><i>{!! $message->user->personaname() !!}</i></b> sent a message <span class="glyphicon glyphicon-envelope"></span>
                            @elseif($message->type === "accept")
                                <b><i>{!! $message->user->personaname() !!}</i></b> accepted your application <span class="glyphicon glyphicon-ok"></span>
                            @elseif($message->type === "reject")
                                <b><i>{!! $message->user->personaname() !!}</i></b> rejected your application <span class="glyphicon glyphicon-remove"></span>
                            @elseif($message->type === "reopen")
                                <b><i>{!! $message->user->personaname() !!}</i></b> reopened your application <span class="glyphicon glyphicon-refresh"></span>
                            @endif
                        </div>
                        <div class="panel-body">
                            <?php global $parser; ?>
                            {!! nl2br(preg_replace("/https?:\/\/([a-zA-Z.]+)[\S]*/", "<a href='$0'>$1</a>", htmlspecialchars($message->message))) !!}
                        </div>
                        <div class="panel-footer">
                            {{$message->created_at}}
                        </div>
                    </div>
                </div>
            @endforeach
            <br><br>
            <form method="post" action="{{URL::to('/join/application/reply/' . $application->id)}}">
                <div class="form-group">
                    <textarea class="form-control" id="text" name="text" placeholder="Discuss or write to us . . ." rows="4" required></textarea>
                </div>
                @permission(["general_admin", "join_admin"])
                    <div class="form-group">
                        <label for="action">
                            <select class="form-control" name="action" id="action" required>
                                <option value="regular" selected>Regular Message</option>
                                <option value="accept">Accept Application</option>
                                <option value="reject">Reject Application</option>
                                @if($application->state === "accepted" || $application->state === "rejected") <option value="reopen">Reopen Application</option> @endif
                            </select>
                        </label>
                    </div>
                @endpermission
                {{csrf_field()}}
                <button type="submit" class="btn btn-success">Reply</button>
            </form>
        </div>
    </div>
    <script>
        function vote(what) {
            window.location = "{{URL::to('/join/application/vote/' . $application->id)}}?what=" + what;
        }
    </script>
@endsection
