@extends("join.layout")

@section("content")
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    Attention!
                </div>
                <div class="panel-body">
                    <p>Please note that we rely on the following things:</p>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">Teamwork</li>
                    <li class="list-group-item">Communication</li>
                    <li class="list-group-item">Calmness</li>
                    <li class="list-group-item">Fairness</li>
                </ul>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-1">
            {!! Form::open(['url' => 'join/now', 'method' => 'post']) !!}
            <div class="form-group">
                <label for="birthday">Birthday</label>
                <input type="text" class="form-control" name="birthday" id="birthday" required>
            </div>
            <div class="form-group">
                <label for="location">Where do you come from?</label>
                <input type="text" class="form-control" name="location" id="location" required>
            </div>
            <div class="form-group">
                <label for="languages">
                    Which languages do you speak?
                </label>
                <input class="form-control" type="text" name="languages" id="languages" required/>
            </div>
            <div class="form-group">
                <label for="text1">
                    Please tell us about yourself (job, school, hobbies, etc):
                </label>
                <textarea class="form-control" id="text1" placeholder=". . ." name="text1" rows="8" required></textarea>
            </div>
            <div class="form-group">
                <label for="text2">
                    Why do you want to become a Moderator?
                </label>
                <textarea class="form-control" id="text2" placeholder=". . ." name="text2" rows="8" required></textarea>
            </div>
            <div class="form-group">
                <label for="text3">
                    How much time can you devote to Moderating per week?
                </label>
                <textarea class="form-control" id="text3" placeholder=". . ." name="text3" rows="3" required></textarea>
            </div>
            <div class="form-group">
                <label for="text4">
                    What are your personal shortcomings or attributes you can improve on?
                </label>
                <textarea class="form-control" id="text4" placeholder=". . ." name="text4" rows="8" required></textarea>
            </div>
            <div class="form-group">
                <label for="text5">
                    Why should we pick you over another applicant?
                </label>
                <textarea class="form-control" id="text5" placeholder=". . ." name="text5" rows="8" required></textarea>
            </div>
            <div class="form-group">
                <label for="text6">
                    Do you have any prior experience in moderating servers or forums? (Please provide references if possible!)
                </label>
                <textarea class="form-control" id="text6" placeholder=". . ." name="text6" rows="8" required></textarea>
            </div>
            <div class="form-group">
                <label for="text7">
                    Do you have any experience with SourceMod, SourceBans, Discord or other skills that could be helpful like coding? (Please provide references if possible!)
                </label>
                <textarea class="form-control" id="text7" placeholder=". . ." name="text7" rows="8" required></textarea>
            </div>
            <p><input type="checkbox" required> I have read the <a href="{{URL::to('/rules/server')}}">server rules</a> and agree with them.</p>
            <p><input type="checkbox" required> I understand that everything which happens in the staff Discord is under a NDA.</p>
            <input type="submit" value="Send Application!" class="btn btn-success">
            {!! Form::close() !!}
        </div>
    </div>

    <script>
        $( function() {
            $( "#birthday" ).datepicker({maxDate: '-13Y'});
        } );
    </script>
@endsection
