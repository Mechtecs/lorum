@extends("join.layout")

@section("content")

    {{ $applications->links() }}

    <div class="panel panel-default">
        <div class="panel-heading">Dashboard</div>
        <table class="table table-hover">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Applicant
                </td>
                <td>
                    State
                </td>
                <td>
                    Age
                </td>
                <td>
                    Status
                </td>
            </tr>
            </thead>
            <tbody id="tablecontent">
            @foreach($applications as $app)
                <tr class="{{($app->state === "accepted") ? "success" : ($app->state === "rejected" ? "danger" : ($app->state === "evaluated" ? "info" : "active"))}}" data-state="{{$app->state}}">
                    <td>
                        {{$app->id}}
                    </td>
                    <td>
                        <a href="/join/application/view/{{$app->id}}">{!! $app->author->personaname !!}</a>
                    </td>
                    <td>
                        {{$app->state}}
                    </td>
                    <td>
                        {{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $app->created_at)->diffForHumans()}}
                    </td>
                    <td>
                        @if($app->allMessagesRead())
                            Nothing New
                        @else
                            <span class="text-info">New Message!!!</span>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    {{ $applications->links() }}

@endsection
