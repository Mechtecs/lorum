body {
    background-image: url("{{$image_url}}");
    background-attachment: fixed;
    background-size: cover;
    background-position: center;
}

a.list-group-item:hover {
    background-color: #303030!important;
}

@media(min-width: 1200px) {
    .container {
        width: 1018px!important;
    }
}

@foreach($ticketTypes as $ticketType)
    @if($ticketType->color_open && $ticketType->color_open !== "#ffffff")
.ticket-type-{{$ticketType->id}}-open {
    background-color: {{$ticketType->color_open}};
}
    @endif
    @if($ticketType->color_closed && $ticketType->color_closed !== "#ffffff")
.ticket-type-{{$ticketType->id}}-closed {
    background-color: {{$ticketType->color_closed}};
}
    @endif
@endforeach
