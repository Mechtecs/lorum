@extends("forum.layout")

@section("title", "Forum")

@section("content")
    @foreach($categories as $category)
        @if($category->hasAccess())
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size: 1.25em;">
                    {{$category->title}}
                </div>
                <table class="table">
                    <tbody>
                        @foreach($category->subcategories as $subcategory)
                            @if($subcategory->hasAccess())
                                <tr>
                                    <td style="width: 70%;">
                                        <a href="{{URL::to('/forum/subcat/' . $subcategory->id)}}" style="font-size: 1.1em;">{{$subcategory->title}}</a>
                                        <div class="text-info" style="font-size: 0.75em;">{{$subcategory->description}}</div>
                                    </td>
                                    <td style="width: 7.5%;">
                                        <span><span class="glyphicon glyphicon-list-alt"></span> {{$subcategory->threadCount()}}</span><BR>
                                    </td>
                                    <td style="width: 22.5%;">
                                        @if(is_null($subcategory->latestThread()))
                                            <span>No threads yet :(</span>
                                        @else
                                            <a href="{{URL::to('/forum/view/' . $subcategory->latestThread()->id)}}">{{$subcategory->latestThread()->shortTitle()}}</a><BR>
                                            <span class="small-text">by {!! $subcategory->latestThread()->latestPost()->author->personaname() !!}</span><BR>
                                            <span class="small-text">posted {{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $subcategory->latestThread()->updated_at)->diffForHumans()}}</span><BR>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    @endforeach
@endsection
