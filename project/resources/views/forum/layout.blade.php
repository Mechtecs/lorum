@extends("app.layout")

@section("container")
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if(isset($links))
        {!! $links->links() !!}
    @endif
    @yield("content")
    @if(isset($links))
        {!! $links->links() !!}
    @endif
@endsection
