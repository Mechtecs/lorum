@extends("forum.layout")

@section("title", $subcategory->title)

@section("content")
    @permission("general_moderator")
        <div id="moderation-dialog" title="Delete Thread">
            <p>Are you sure you want to delete this thread?</p>
        </div>
        <div id="moderation-undelete-dialog" title="Un-Delete Thread">
            <p>Are you sure you want to <i>un-delete</i> this thread?</p>
        </div>
        <div id="moderation-move-dialog" title="Move Thread">
            <select id="moderation-move-select">
                @foreach($categories as $category)
                    @foreach($category->subcategories as $subcat)
                        <option value="{{$subcat->id}}">{{$category->title}} - {{$subcat->title}}</option>
                    @endforeach
                @endforeach
            </select>
        </div>
    @endpermission

    <div class="panel panel-default">
        <div class="panel-heading" style="font-size: 1.15em;">
            {{$subcategory->title}}
        </div>
        <table class="table">
            @foreach($threads as $thread)
                @if($thread->latestPost()->author !== null)
                    <tr id="thread-tr-{{$thread->id}}" @if($thread->trashed()) class="info" @endif >
                        <td style="width: 70%;">
                            <a href="{{URL::to('/forum/view/' . $thread->id)}}" style="font-size: 1.1em;">{{$thread->title}}</a><BR>
                            <span class="small-text">by {!! $thread->author->personaname() !!} posted on {!! \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $thread->created_at)->toFormattedDateString() !!}</span><BR>
                        </td>
                        <td style="width: 7.5%;">
                            <span><span class="glyphicon glyphicon-comment"></span> {{$thread->posts()->count()}}</span><BR>
                        </td>
                        <td style="width: 22.5%;">
                            <span>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $thread->updated_at)->diffForHumans()}}</span><BR>
                            <span class="small-text">by {!! $thread->latestPost()->author->personaname() !!}</span><BR>
                        </td>
                        @permission("general_moderator")
                            @if($thread->trashed())
                                <td>
                                    <span class="glyphicon glyphicon-refresh" onclick="openUndeleteMenu(this, {{$thread->id}})"></span>
                                </td>
                            @else
                                <td>
                                    <span class="glyphicon glyphicon-remove" onclick="openSubCatMenu(this, {{$thread->id}})"></span>
                                    <span class="glyphicon glyphicon glyphicon-move" onclick="openSubCatMoveMenu(this, {{$thread->id}})"></span>
                                </td>
                            @endif
                        @endpermission
                    </tr>
                @endif
            @endforeach
        </table>
        @if($subcategory->canCreateThread())
            <div class="panel-footer">
                <a href="{{URL::to('/forum/subcat/' . $subcategory->id . '/new')}}" class="btn btn-sm btn-success">New Thread</a>
            </div>
        @endif
    </div>

    @permission("general_moderator")
        <script>
            var thread_id = null;

            jQuery(function() {
                $("#moderation-dialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 'auto',
                    width: 'auto',
                    buttons: {
                        "Delete": function() {
                            window.location = "{{URL::to('/admin/forum/post')}}/" + thread_id + "/delete";
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });

                $("#moderation-move-dialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 'auto',
                    width: 'auto',
                    buttons: {
                        "Move": function() {
                            window.location = "{{URL::to('/admin/forum/post')}}/" + thread_id + "/move?subcategory=" + $("#moderation-move-select :selected").attr('value');
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });

                $("#moderation-undelete-dialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 'auto',
                    width: 'auto',
                    buttons: {
                        "Un-Delete": function() {
                            window.location = "{{URL::to('/admin/forum/post')}}/" + thread_id + "/undelete";
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });
            });

            function openMenu(selector, button) {
                $(selector).dialog("option", "position", {my: 'right top', at: 'left bottom', of: $(button)});
                $(selector).dialog("open");
            }

            function openSubCatMenu(button, tid) {
                thread_id = tid;
                openMenu("#moderation-dialog", button);
            }

            function openSubCatMoveMenu(button, tid) {
                thread_id = tid;
                openMenu("#moderation-move-dialog", button);
            }

            function openUndeleteMenu(button, tid) {
                thread_id = tid;
                openMenu("#moderation-undelete-dialog", button);
            }
        </script>
    @endpermission
@endsection
