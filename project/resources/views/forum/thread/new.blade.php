@extends("forum.layout")

@section("title", "New Thread - " . $subcategory->title)

@section("content")
    @if (count($errors) > 0)
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    <form action="{{URL::to('/forum/subcat/' . $subcategory->id . '/new')}}" method="post">
        {{csrf_field()}}
        <div class="form-group-sm">
            <label for="title">Title:</label>
            <input type="text" name="title" id="title" class="form-control" placeholder="My awesome title . . ." value="{{old("title")}}">
        </div>
        <div class="margin-top margin-bottom">
            <textarea name="text" id="text" rows="10" cols="80">
                {{old("text")}}
            </textarea>
        </div>
        <div class="btn-group">
            <input class="btn btn-success" type="submit" value="POST IT!">
        </div>
    </form>

    <script>
        CKEDITOR.replace('text');
    </script>
@endsection
