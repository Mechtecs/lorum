@extends("forum.layout")

@section("title", $thread->title . " - " . $thread->sub_category->title)

@section("content")
    <div class="panel panel-default">
        <div class="panel-heading">{{$thread->title}} by {!! $thread->author->personaname() !!}</div>
    </div>

    @foreach($posts as $post)
        <div class="panel panel-default">
            <div class="panel-heading">
                <span>
                    {!! $post->author->personaname() !!}
                </span>
                <span class="pull-right">
                    {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at)->toDayDateTimeString()}}
                </span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        @if($post->author->isFake())
                            <span>This is an old Forum account which is not linked to Steam.</span>
                        @else
                            <img src="{{$post->author->avatarfull}}" class="img-responsive img-rounded">
                            <span>{{$post->author->roles()->first()->display_name}}</span><BR>
                            @if($post->author->getKrakenStats("playtime") > 0)
                              <span>Playtime: {{$post->author->getKrakenStats("playtime")}} minutes</span><BR>
                            @endif
                            @if($post->author->isOnline())
                                <span>Currently playing on <a href="steam://connect/{{$post->author->lastServer()->ip}}:{{$post->author->lastServer()->port}}">{{$post->author->lastServer()->name}}</a></span>
                            @endif
                        @endif
                    </div>
                    <div class="col-md-10">
                        @if($post->isEditable())
                            <textarea id="post-content-{{$post->id}}" contenteditable="true" name="content" style="display: none;">
                                {{$post->content}}
                            </textarea>
                        @endif
                        <div id="post-visible-{{$post->id}}">
                            <?php global $parser; ?>
                            {!! preg_replace_callback("/((href|src)=[\"']|)(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i", function($matches) {
                                return (($matches[1]) ? $matches[0] : "<a href=\"" . $matches[3] . "\">" . $matches[3] . "</a>");
                            }, nl2br($parser->except('youtube')->parse(htmlspecialchars($post->content)))) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                @if($post->isEditable())
                    <input type="button" value="Edit" class="btn btn-warning" onclick="edit({{$post->id}})" id="save-edit-{{$post->id}}">
                    <div class="input-group" style="display: none;" id="save-change-{{$post->id}}">
                        <span class="input-group-btn">
                            <input type="button" value="Save Changes" class="btn btn-warning" onclick="saveEdit({{$post->id}})" id="post-edit-button-{{$post->id}}">
                        </span>
                        <input type="text" id="post-reason-{{$post->id}}" placeholder="What did you change?" class="form-control">
                    </div>
                @endif
                @if($post->editor_id)
                    <span>edited by: {!! $post->editor->personaname() !!} - {{$post->edit_reason}} @ {{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $thread->updated_at)->toTimeString()}}</span>
                @endif
            </div>
        </div>
    @endforeach
    @if($thread->sub_category->canReplyToThreads())
        <div class="panel panel-default">
            <div class="panel-heading">
                Reply to this thread
            </div>
            <div class="panel-content">
                <textarea id="reply-content">

                </textarea>
            </div>
            <div class="panel-footer">
                <input type="button" value="Reply" class="btn btn-success" onclick="reply(this)">
            </div>
        </div>
    @endif
    <script>
        function edit(postid, button) {
            $("#save-edit-" + postid).remove();
            $("#save-change-" + postid).show();
            $("#post-visible-" + postid).hide();

            CKEDITOR.disableAutoInline = true;
            CKEDITOR.replace('post-content-' + postid);
        }

        function saveEdit(postid) {
            var text = CKEDITOR.instances['post-content-' + postid].getData();
            var reason = $("#post-reason-" + postid).val();

            console.log(text);
            console.log(reason);

            $("#post-edit-button-" + postid).prop('disabled', true);

            $.post("{{URL::to('/forum/post/')}}/" + postid + "/edit", {text: text, reason: reason}, function(data, status) {
                console.log(status);
                if(status === "success") {
                    $("#post-edit-button-" + postid).prop('disabled', false);
                    if(data.success) {
                        alert("Successfully edited your post!")
                        location.reload(true);
                    } else {
                        alert("Error: " + data.reason);
                    }
                }
            });
        }

        function reply(button) {
            $(button).prop("disabled", true);
            var text = CKEDITOR.instances["reply-content"].getData();

            $.post("{{URL::to('/forum/post/')}}/reply", {text: text, thread_id: {{$thread->id}}}, function(data, status) {
                if(status === "success") {
                    if(data.success)
                        location.reload(true);
                    else
                        alert(data.reason);
                }
            });
        }

        $(function() {
            CKEDITOR.replace('reply-content');
        });
    </script>
@endsection
