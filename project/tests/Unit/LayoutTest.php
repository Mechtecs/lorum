<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LayoutTest extends TestCase
{
    use DatabaseTransactions;

    public function testLoginButton() {
        $resp = $this->get('/forum');

        $resp->assertSee('Login');

        $user = factory(User::class)->create();

        $this->actingAs($user);
        $resp = $this->get('/forum');

        $resp->assertSee('Profile');
        $resp->assertSee('Settings');
        $resp->assertSee('Logout');
        $resp->assertSee($user->personaname());
    }

    public function testLayout() {
        $resp = $this->get('/forum');
        $resp->assertSee('Forum');
        $resp->assertSee('Bans');
        $resp->assertSee('This software is a product of the 3kliksphilip Community. Contribute on <a href="https://gitlab.com/Mechtecs/lorum">GitLab</a>. &copy;');
    }
}
