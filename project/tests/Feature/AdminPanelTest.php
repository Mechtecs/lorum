<?php

namespace Tests\Feature;

use App\Role;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminPanelTest extends TestCase
{
    use DatabaseTransactions;


    /**
     * @return Role
     */
    private function getAdminRole() {
        return Role::where('name', '=', 'administrator')->first();
    }

    public function testVisibleByAdmin() {
        $adminrole = $this->getAdminRole();
        $user = factory(User::class)->create();
        $user->attachRole($adminrole);

        $this->actingAs($user);

        $resp = $this->get('/admin');

        $resp->assertStatus(200);
    }

    public function testNotVisibleByOtherGroups() {
        foreach(Role::where('name', '!=', 'administrator')->where('name', '!=', 'moderator')->get()->all() as $role) {
            $user = factory(User::class)->create();
            $user->attachRole($role);

            $this->actingAs($user);

            $resp = $this->get('/admin');

            $resp->assertStatus(403);
        }
    }

    public function testPagesGoSuccess() {
        $user = factory(User::class)->create();
        $adminrole = $this->getAdminRole();
        $user->attachRole($adminrole);
        $this->actingAs($user);

        foreach(array("/admin/users", "/admin/roles", "/admin/forums") as $v) {
            $resp = $this->get($v);
            if($resp->status() != 200)
                echo "Page: " . $v . "\n";
            $resp->assertStatus(200);
        }
    }
}
