-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 30. Jan 2017 um 01:36
-- Server-Version: 10.1.16-MariaDB
-- PHP-Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Datenbank: `lorum`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` char(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `categories`
--

INSERT INTO `categories` (`id`, `title`, `description`, `order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'General\r\n', NULL, 0, '2017-01-29 23:00:00', '2017-01-29 23:00:00', '2017-01-29 23:00:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `category_role`
--

CREATE TABLE `category_role` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `can_view` tinyint(1) NOT NULL,
  `can_new_thread` tinyint(1) NOT NULL,
  `can_reply_thread` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2017_01_29_172911_entrust_setup_tables', 1),
(3, '2017_01_29_180907_forum_tables', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'show_on_memberlist', 'Show users of this group on the member list.', 'Show users of this group on the member list.', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(2, 'show_on_teamlist', 'Show users of this group on the team page', 'Show users of this group on the team page', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(3, 'is_banned_group', 'Users in this group are banned', 'Users in this group are banned', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(4, 'general_moderator', 'Users of this group can access the moderator CP', 'Users of this group can access the moderator CP', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(5, 'general_admin', 'Users of this group can access the admin CP', 'Users of this group can access the admin CP', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(6, 'view_board', 'Can view board?', 'Can view board?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(7, 'view_threads', 'Can view threads?', 'Can view threads?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(8, 'view_profile', 'Can view user profiles?', 'Can view user profiles?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(9, 'download_attatchments', 'Can download attatchments?', 'Can download attatchments?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(10, 'view_board_when_closed', 'Can view board when closed?', 'Can view board when closed?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(11, 'post_new_threads', 'Can post new threads?', 'Can post new threads?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(12, 'post_replies', 'Can post replies to threads?', 'Can post replies to threads?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(13, 'post_attatchments', 'Can post attatchments?', 'Can post attatchments?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(14, 'post_edit_own', 'Can edit own posts?', 'Can edit own posts?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(15, 'post_delete_own', 'Can delete own posts?', 'Can delete own posts?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(16, 'post_delete_threads', 'Can delete own threads?', 'Can delete own threads?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(17, 'post_delete_attatchments_own', 'Can delete own attatchments?', 'Can delete own attatchments?', '2017-01-29 18:09:59', '2017-01-29 18:09:59'),
(18, 'reportable', 'User can be reported?', 'User can be reported?', '2017-01-29 18:09:59', '2017-01-29 18:09:59');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `thread_id` int(11) NOT NULL,
  `user_id` char(17) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `edit_reason` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username_style` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '{username}',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `role_user`
--

CREATE TABLE `role_user` (
  `user_id` char(17) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` char(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `subcategories`
--

INSERT INTO `subcategories` (`id`, `title`, `description`, `order`, `category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Announcements', 'Rules and important announcements.', 0, 1, '2017-01-29 23:42:02', '2017-01-29 23:42:02', '2017-01-29 23:42:02'),
(2, 'Introduce Yourself', 'New here?, introduce yourself and become part of the community.\r\n', 1, 1, '2017-01-29 23:42:45', '2017-01-29 23:42:45', '2017-01-29 23:42:45');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `threads`
--

CREATE TABLE `threads` (
  `id` int(10) UNSIGNED NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `user_id` char(17) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `steamid64` char(17) COLLATE utf8mb4_unicode_ci NOT NULL,
  `personaname` char(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profileurl` text COLLATE utf8mb4_unicode_ci,
  `avatar` text COLLATE utf8mb4_unicode_ci,
  `avatarmedium` text COLLATE utf8mb4_unicode_ci,
  `avatarfull` text COLLATE utf8mb4_unicode_ci,
  `personastate` smallint(6) DEFAULT NULL,
  `communityvisibilitystate` smallint(6) DEFAULT NULL,
  `profilestate` tinyint(1) DEFAULT NULL,
  `lastlogoff` timestamp NULL DEFAULT NULL,
  `commentpermission` tinyint(1) DEFAULT NULL,
  `realname` text COLLATE utf8mb4_unicode_ci,
  `primaryclanid` text COLLATE utf8mb4_unicode_ci,
  `timecreated` timestamp NULL DEFAULT NULL,
  `gameid` int(11) DEFAULT NULL,
  `gameserverip` char(21) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gameextrainfo` text COLLATE utf8mb4_unicode_ci,
  `loccityid` int(11) DEFAULT NULL,
  `locstatecode` char(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loccountrycode` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `category_role`
--
ALTER TABLE `category_role`
  ADD PRIMARY KEY (`category_id`,`role_id`),
  ADD KEY `category_role_role_id_foreign` (`role_id`);

--
-- Indizes für die Tabelle `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indizes für die Tabelle `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indizes für die Tabelle `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indizes für die Tabelle `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indizes für die Tabelle `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `users_steamid64_unique` (`steamid64`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT für Tabelle `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `threads`
--
ALTER TABLE `threads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `category_role`
--
ALTER TABLE `category_role`
  ADD CONSTRAINT `category_role_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`steamid64`) ON DELETE CASCADE ON UPDATE CASCADE;
