<?php

use App\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSettingsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create("settings", function (Blueprint $table) {
      $table->increments("id");
      $table->char("key", 64);
      $table->string("value", 2048);
      $table->timestamps();
    });

    Setting::setSetting("title", "3kliksphilip Community");
    Setting::setSetting("projectUrl", "https://gitlab.com/Mechtecs/lorum");
    Setting::setSetting("projectUrlName", "GitLab");
    Setting::setSetting("banUrl", "https://sb.3kliksphilip.com/");
    Setting::setSetting("privacyPolicy", "<a href=\"//www.iubenda.com/privacy-policy/7899149\" class=\"iubenda-white iubenda-embed\" title=\"Privacy Policy\">Privacy Policy</a><script type=\"text/javascript\">(function (w,d) {var loader = function () {var s = d.createElement(\"script\"), tag = d.getElementsByTagName(\"script\")[0]; s.src = \"//cdn.iubenda.com/iubenda.js\"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener(\"load\", loader, false);}else if(w.attachEvent){w.attachEvent(\"onload\", loader);}else{w.onload = loader;}})(window, document);</script>");
    Setting::setSetting("sponsor", "<a href=\"https://pulseservers.com\"><img src=\"/img/pulse-servers.png\"></a>");
    Setting::setSetting("header", "<script type=\"text/javascript\">
            var _paq = _paq || [];
            @if(!Auth::guest())
                _paq.push(['setUserId', '{{Auth::user()->steamid64}}']);
            @endif
            _paq.push([\"setDocumentTitle\", document.domain + \"/\" + document.title]);
            _paq.push([\"setCookieDomain\", \"*.lorum.eu.3kliksphilip.com\"]);
            _paq.push([\"setDomains\", [\"*.lorum.eu.3kliksphilip.com\"]]);
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
                var u=\"https://piwik.eu.3kliksphilip.com/\";
                _paq.push(['setTrackerUrl', u+'piwik.php']);
                _paq.push(['setSiteId', '2']);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
            })();
        </script>
        <noscript><p><img src=\"https://piwik.eu.3kliksphilip.com/piwik.php?idsite=2&rec=1\" style=\"border:0;\" alt=\"\" /></p></noscript>");
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop("settings");
  }
}
