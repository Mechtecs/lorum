<?php
use App\Permission;
use App\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EntrustSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing roles
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 128)->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->string('image')->nullable();
            $table->string('username_style')->default('{username}');
            $table->timestamps();
        });

        // Create table for associating roles to users (Many-to-Many)
        Schema::create('role_user', function (Blueprint $table) {
            $table->char('steamid64_id', 17);
            $table->integer('role_id')->unsigned();

            $table->foreign('steamid64_id')->references('steamid64')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['steamid64_id', 'role_id']);
        });

        // Create table for storing permissions
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 128)->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('permission_role', function (Blueprint $table) {
            $table->integer('permission_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('permission_id')->references('id')->on('permissions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['permission_id', 'role_id']);
        });

        $permissions = array(
            'show_on_memberlist' => 'Show users of this group on the member list.',
            'show_on_teamlist' => 'Show users of this group on the team page',
            'is_banned_group' => 'Users in this group are banned',
            'general_moderator' => 'Users of this group can access the moderator CP',
            'general_admin' => 'Users of this group can access the admin CP',
            'view_board' => 'Can view board?',
            'view_threads' => 'Can view threads?',
            'view_profile' => 'Can view user profiles?',
            'download_attatchments' => 'Can download attatchments?',
            'view_board_when_closed' => 'Can view board when closed?',
            'post_new_threads' => 'Can post new threads?',
            'post_replies' => 'Can post replies to threads?',
            'post_attatchments' => 'Can post attatchments?',
            'post_edit_own' => 'Can edit own posts?',
            'post_delete_own' => 'Can delete own posts?',
            'post_delete_threads' => 'Can delete own threads?',
            'post_delete_attatchments_own' => 'Can delete own attatchments?',
            'post_ban_appeal' => 'Can post ban appeals?',
            'reportable' => 'User can be reported?');
        $permissionObjs = array();
        foreach($permissions as $name => $desc) {
            $permission = new Permission();
            $permission->name = $name;
            $permission->display_name = $desc;
            $permission->description = $desc;
            $permission->saveOrFail();
            $permissionObjs[$name] = $permission;
        }

        $admin = new Role();
        $admin->name = "administrator";
        $admin->display_name = "Administrator";
        $admin->description = "This role has the power to do EVERYTHING!";
        $admin->save();
        $admin->attachPermissions($permissionObjs);
        $admin->detachPermission("reportable");

        $moderator = new Role();
        $moderator->name = "moderator";
        $moderator->display_name = "Moderator";
        $moderator->description = "This role should be given to users with moderating powers.";
        $moderator->save();
        foreach(array(
                    "show_on_memberlist", "show_on_teamlist", "is_banned_group", "general_moderator",
                    "view_board", "view_threads", "view_profile", "download_attatchments", "view_board_when_closed",
                    "post_new_threads", "post_replies", "post_attachments", "post_edit_own", "post_delete_own",
                    "post_delete_threads", "post_delete_attatchments_own", "reportable") as $k) {
            $perm = Permission::where('name', '=', $k)->first();
            $moderator->attachPermission($perm);
        }

        $user = new Role();
        $user->name = "user";
        $user->display_name = "User";
        $user->description = "A normal user. Meh.";
        $user->save();
        foreach(array(
                    "show_on_memberlist", "view_board", "view_threads", "view_profile",
                    "download_attatchments", "post_new_threads", "post_replies", "post_edit_own",
                    "reportable", "post_ban_appeal") as $k) {
            $perm = Permission::where('name', '=', $k)->first();
            $user->attachPermission($perm);
        }

        $banned = new Role();
        $banned->name = "banned";
        $banned->display_name = "Banned";
        $banned->description = "This user is banned.";
        $banned->save();
        foreach(array(
                    "view_board", "view_threads", "view_profile",
                    "download_attatchments", "reportable", "post_ban_appeal") as $k) {
            $perm = Permission::where('name', '=', $k)->first();
            $banned->attachPermission($perm);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('permission_role');
        Schema::drop('permissions');
        Schema::drop('role_user');
        Schema::drop('roles');
    }
}
