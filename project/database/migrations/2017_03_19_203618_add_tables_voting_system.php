<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablesVotingSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_votes', function(Blueprint $table) {
            $table->increments('id');
            $table->char('user_id', 17);
            $table->unsignedInteger('application_id');
            $table->enum('for', array('yes', 'no', 'meh'))->default('meh');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('application_votes');
    }
}
