<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToSpecialPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("subcategory_role_permissions", function(Blueprint $table) {
            $table->boolean('read_only_own');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("subcategory_role_permissions", function(Blueprint $table) {
            $table->dropColumn('read_only_own');
        });
    }
}
