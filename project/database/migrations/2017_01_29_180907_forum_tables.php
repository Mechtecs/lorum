<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForumTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function(Blueprint $table) {
            $table->integer('id', true, true);
            $table->char('title', 64)->default("");
            $table->text('description')->nullable();
            $table->integer('order');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('subcategories', function(Blueprint $table) {
            $table->integer('id', true, true);
            $table->char('title', 64)->default("");
            $table->text('description')->nullable();
            $table->integer('order')->default(0);
            $table->integer('category_id');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('threads', function(Blueprint $table) {
            $table->integer('id', true, true);
            $table->integer('subcategory_id');
            $table->char('user_id', 17);
            $table->char('title', 64);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('posts', function(Blueprint $table) {
            $table->integer('id', true, true);
            $table->integer('thread_id');
            $table->char('user_id', 17);
            $table->text('content');
            $table->text('edit_reason')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('category_role', function(Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->boolean('can_view');
            $table->boolean('can_new_thread');
            $table->boolean('can_reply_thread');
            $table->primary(array('category_id', 'role_id'));
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach(array('category_role', 'categories', 'subcategories', 'threads', 'posts') as $v)
            Schema::drop($v);
    }
}
