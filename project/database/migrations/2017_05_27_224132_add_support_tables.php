<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupportTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("tickets", function(Blueprint $table) {
            $table->increments("id");
            $table->char('author_id', 17);
            $table->integer('ticket_type_id');
            $table->enum("state", array("open", "closed"));
            $table->timestamps();
        });

        Schema::create("ticket_fields", function(Blueprint $table) {
            $table->increments("id");
            $table->char("author_id", 17);
            $table->integer("ticket_id");
            $table->integer("ticket_type_field_id");
            $table->string("value");
            $table->timestamps();
        });

        Schema::create("ticket_messages", function(Blueprint $table) {
            $table->increments("id");
            $table->integer("ticket_id");
            $table->char("author_id", 17);
            $table->string("message", 4096);
            $table->timestamps();
        });

        Schema::create("ticket_types", function(Blueprint $table) {
            $table->increments("id");
            $table->string("name", 128);
            $table->string("description", 512)->nullable();
            $table->integer("order");
            $table->timestamps();
        });

        Schema::create("ticket_type_roles", function(Blueprint $table) {
            $table->increments("id");
            $table->integer("ticket_type_id");
            $table->integer("role_id");
            $table->timestamps();
        });

        Schema::create("ticket_type_fields", function(Blueprint $table) {
            $table->increments("id");
            $table->integer("ticket_type_id");
            $table->string("name", 128);
            $table->string("description", 512)->nullable();
            $table->enum("type", array("string", "textarea", "integer", "date", "password", "steamid", "serverid", "format_view"));
            $table->integer("order");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach(array("tickets", "ticket_fields", "ticket_messages", "ticket_types", "ticket_type_roles", "ticket_type_fields") as $item)
            Schema::drop($item);
    }
}
