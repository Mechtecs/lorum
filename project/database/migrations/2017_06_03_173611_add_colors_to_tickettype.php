<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColorsToTickettype extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("ticket_types", function(Blueprint $table) {
            $table->char('color_open', 7)->default('#ffffff')->after('description');
            $table->char('color_closed', 7)->default('#ffffff')->after('color_open');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("ticket_types", function(Blueprint $table) {
            $table->dropColumn('color_open');
            $table->dropColumn('color_closed');
        });
    }
}
