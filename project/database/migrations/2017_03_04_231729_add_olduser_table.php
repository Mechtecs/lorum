<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOlduserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("old_users", function(Blueprint $table) {
            $table->increments("id");
            $table->integer("uid")->unique();
            $table->char("username", 120)->unique();
            $table->char("password", 120);
            $table->char("salt", 10);
            $table->char("loginkey", 50);
            $table->char("email", 220);
            $table->boolean("linked")->default(false);
            $table->char("steamid64", 17)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("old_users");
    }
}
