<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->char('steamid64', 17)->unique()->index();
            $table->char('personaname', 64)->nullable();
            $table->text('profileurl')->nullable();
            $table->text('avatar')->nullable();
            $table->text('avatarmedium')->nullable();
            $table->text('avatarfull')->nullable();
            $table->smallInteger('personastate')->nullable();
            $table->smallInteger('communityvisibilitystate')->nullable();
            $table->boolean('profilestate')->nullable();
            $table->timestamp('lastlogoff')->nullable();
            $table->boolean('commentpermission')->nullable();
            $table->text('realname')->nullable();
            $table->text('primaryclanid')->nullable();
            $table->timestamp('timecreated')->nullable();
            $table->integer('gameid')->nullable();
            $table->char('gameserverip', 21)->nullable();
            $table->text('gameextrainfo', 64)->nullable();
            $table->integer('loccityid')->nullable();
            $table->char('locstatecode', 4)->nullable();
            $table->char('loccountrycode', 2)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
