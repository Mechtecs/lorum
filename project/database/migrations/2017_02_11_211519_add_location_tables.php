<?php

use App\LocCity;
use App\LocCountry;
use App\LocState;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loc_cities', function(Blueprint $table) {
            $table->increments("id");
            $table->char("city_id", 32);
            $table->string("name");
            $table->string("coordinates")->nullable();
            $table->string("accuracy_level")->nullable();
            $table->integer("state_id");
            $table->index("state_id");
            $table->index("city_id");
            $table->timestamps();
        });

        Schema::create('loc_states', function(Blueprint $table) {
            $table->increments("id");
            $table->char("state_id", 32);
            $table->string("name");
            $table->string("coordinates")->nullable();
            $table->string("accuracy_level")->nullable();
            $table->integer("country_id");
            $table->index("country_id");
            $table->index("state_id");
            $table->timestamps();
        });

        Schema::create('loc_countries', function(Blueprint $table) {
            $table->increments("id");
            $table->char("country_id", 2)->unique();
            $table->string("name");
            $table->string("coordinates")->nullable();
            $table->string("accuracy_level")->nullable();
            $table->timestamps();
        });

        $jsondata = json_decode(file_get_contents(__DIR__.'/../../resources/json/steam_countries.min.json'), true);

        foreach($jsondata as $country_id => $countryobj) {

            $country = new LocCountry();
            $country->country_id = $country_id;
            $country->name = trim($countryobj["name"]);
            if(isset($countryobj["coordinates"])) {
                $country->coordinates = $countryobj["coordinates"];
                $country->accuracy_level = $countryobj["coordinates_accuracy_level"];
            }
            $country->save();

            foreach($countryobj["states"] as $state_id => $stateobj) {

                $state = new LocState();
                $state->state_id = $state_id;
                $state->name = trim($stateobj["name"]);
                if(isset($stateobj["coordinates"])) {
                    $state->coordinates = $stateobj["coordinates"];
                    $state->accuracy_level = $stateobj["coordinates_accuracy_level"];
                }
                $state->country_id = $country->id;
                $state->save();

                foreach($stateobj["cities"] as $city_id => $cityobj) {
                    $city = new LocCity();
                    $city->city_id = $city_id;
                    $city->name = trim($cityobj["name"]);
                    if(isset($cityobj["coordinates"])) {
                        $city->coordinates = $cityobj["coordinates"];
                        $city->accuracy_level = $cityobj["coordinates_accuracy_level"];
                    }
                    $city->state_id = $state->id;
                    $city->save();
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("loc_cities");
        Schema::drop("loc_states");
        Schema::drop("loc_countries");
    }
}
