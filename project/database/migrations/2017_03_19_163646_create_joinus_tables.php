<?php

use App\Permission;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJoinusTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function(Blueprint $table) {
            $table->increments('id');
            $table->text('languages');
            $table->text('location');
            $table->date('birthday');
            for($i = 1; $i < 8; $i++)
                $table->text('text' . $i);
            $table->enum('state', array('opened', 'accepted', 'rejected', 'reopened'))->default('opened');
            $table->char('author_id', 17);
            $table->timestamps();
        });

        Schema::create('application_messages', function(Blueprint $table) {
            $table->increments('id');
            $table->char('author_id', 17);
            $table->text('message');
            $table->enum('type', array('regular', 'reopen', 'reject', 'accept'))->default('regular');
            $table->unsignedInteger('application_id')->index();
            $table->timestamps();
        });

        Schema::create('application_message_read', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('application_message_id');
            $table->char('user_id', 17);
            $table->index(array('user_id', 'application_message_id'));
            $table->timestamps();
        });

        $permission = new Permission();
        $permission->name = "join_admin";
        $permission->display_name = "Application Admin";
        $permission->description = "Can moderate the applications.";
        $permission->saveOrFail();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("applications");
        Schema::drop("application_messages");
        Schema::drop("application_message_read");
        Permission::where('name', '=', 'join_admin')->first()->delete();
    }
}
