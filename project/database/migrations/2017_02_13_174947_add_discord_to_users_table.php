<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscordToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("users", function(Blueprint $table) {
            $table->char("discord_id", 18)->nullable();
            $table->string("discord_username")->nullable();
            $table->string("discord_email")->nullable();
            $table->char("discord_discriminator", 5)->nullable();
            $table->boolean("discord_verified")->nullable();
            $table->boolean("discord_mfa_enabled")->nullable();
            $table->string("discord_token");
            $table->boolean("discord_link")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("users", function(Blueprint $table) {
            $table->dropColumn(array("discord_id", "discord_username", "discord_email", "discord_discriminator",
                "discord_verified", "discord_mfa_enabled", "discord_link", "discord_token"));
        });
    }
}
