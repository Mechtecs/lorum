import DateTimeFormat = Intl.DateTimeFormat;
/**
 * Created by max on 30.01.2017.
 */
export class Category {
    constructor(
        public id: number,
        public title: string,
        public description: string,
        public order: number,
        public created_at: DateTimeFormat,
        public updated_at: DateTimeFormat,
        public deleted_at: DateTimeFormat,
        public subcategories: Category,
    ) {
        console.log(this);
    }
}