import { Component, OnInit } from '@angular/core';
import {CategoryService} from "../Services/categories.service";
import {Category} from "../Models/category";

@Component({
    selector: 'ng-categories',
    templateUrl: 'categories.component.html',
    styleUrls: ['categories.component.css']
})
export class CategoriesComponent implements OnInit {

    categories: Category[];

    constructor(private categoryService: CategoryService) { }

    loadComments() {
        this.categoryService.getCategories()
            .subscribe(
                categories => this.categories = categories
            );
    }

    ngOnInit() {
        this.loadComments();
    }

}
